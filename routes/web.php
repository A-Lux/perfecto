<?php

use Illuminate\Support\Facades\Route;

use Illuminate\Http\Request;
use App\Category;
use App\Subcategory;
use App\Item;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index')->name('main');

Route::get('/delivery', function() {
	return view('delivery.index');
});

Route::get('/exchange', function() {
	return view('exchange.index');
});

Route::get('/categories', function() {
	return view('categories.index');
});

Route::get('/about', function() {
	return view('about.index');
});

Route::get('/paper', function() {
	return view('paper.index');
});

Route::get('/entry', function() {
	return view('entry.index');
});

Route::get('/action', function() {
	$items = Item::inRandomOrder()->limit(5)->get();
	return view('action.index', compact('items'));
});

Route::get('/card-things', function() {
	return view('card-things.index');
});

Route::get('/cabinet', function() {
	return view('cabinet.index');
});

Route::get('/cabinet-date', function() {
	return view('cabinet-date.index');
});

Route::get('/cabinet-pass', function() {
	return view('cabinet-pass.index');
});

Route::get('/cabinet-mail', function() {
	return view('cabinet-mail.index');
});

Route::get('/cabinet-mail-rep', function() {
	return view('cabinet-mail-rep.index');
});

Route::get('/posts', function() {
	return view('basket.index');
});

Route::get('/catalog/all', 'MainController@catalog_all')->name('catalog-all');

// Route::get('/catalog/all', [MainController:class,'catalog_all']);

Route::get('/catalog/{id}', 'MainController@catalog')->name('category');
Route::get('/basket', 'MainController@basket')->name('basket');
Route::get('/item/{id}', 'ItemController@show')->name('item');

Route::get('/posts', 'ArticleController@index');
Route::get('/post/{id}', 'ArticleController@show');
Route::get('/posts/all', 'ArticleController@all');

Route::get('/contacts', function() {
	return view('contacts.index');
});

Route::get('/test/{id}', function($id){
	$categories = Category::getWomanCategories();
	$currentCategory = Subcategory::find($id);
	$items = Item::where('category_id', $id)->paginate(12);
	$filters = App\Filter::where('category_id', $currentCategory->category_id)->get();
	return view('categories.show-test', compact('categories', 'currentCategory', 'items', 'filters'));
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
