<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Resources\CategoryCollection;

use App\Http\Resources\PostCollection;
use App\Http\Resources\SaleCategoryCollection;
use App\Http\Resources\Post as PostResource;
use App\Http\Resources\SliderCollection;

use App\Http\Controllers\PassportAuthController;
use App\Http\Controllers\PostController;
use App\Http\Resources\ItemCollection;

use App\Category;
use App\Article;
use App\SaleCategory;
use App\Subcategory;
use App\Sale;
use App\SliderMain;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//Categories
Route::get('categories', function() {
    return new CategoryCollection(Category::getCategories());
});

Route::get('subcategories/{id}', function($id) {
    $category = Category::find($id);
    return new CategoryCollection($category->subcategories);
});

Route::get('category/items/{category_id}', function($category_id) {
    return new ItemCollection(Subcategory::find($category_id)->items);
});

//Items
Route::get('items', 'ItemApiController@index');
Route::get('items/man', 'ItemApiController@man');
Route::get('items/woman', 'ItemApiController@woman');
Route::get('item/{id}', 'ItemApiController@show');
Route::get('items/popular', 'ItemApiController@popular');
Route::get('items/new', 'ItemApiController@new');
Route::get('item/issale/{id}', 'ItemApiController@isSale');

//Sales
Route::get('sales', function() {
    return new SaleCategoryCollection(SaleCategory::all());
});

Route::get('sale/{id}', function($id) {
    $category = SaleCategory::find($id);
    $ids = $category->getSalesItemsIds();
    return new ItemCollection(Item::whereIn('id', $ids)->get());
});

//Posts
Route::get('posts/', function() {
    return new PostCollection(Article::all());
});

Route::get('post/{id}', function($id) {
    return new PostResource(Article::find($id));
});

Route::post('register', 'PassportAuthController@user_register');
Route::post('login', 'PassportAuthController@login');

//Wishlist
Route::middleware('auth:api')->group(function () {
    Route::post('/wishlist/add', 'WishlistController@store');
    Route::get('/wishlist', 'WishlistController@index');
    Route::get('/wishlist/delete/{id}', 'WishlistController@destroy');
});

//Slider
Route::get('slider', function() {
    return new SliderCollection(SliderMain::all());
});

Route::get('filters/{category_id}','FilterController@index');
Route::get('filter/{id}','FilterController@show');
Route::post('sort','FilterController@sort');
Route::get('sort/price','FilterController@sort_price');

Route::get('dd', function(Request $request) {
    dd($request->all());
});
//user - бонусы