<div style="width:100%">
    <div class="catalog-filter-select-thing">
        <div class="catalog-select-block-items">

            <div class="select-items-drop">
                <span class='openselect'>Сортировать</span> <span></span>
                <div class="select-element-block invisiblewin" id='select-sort'>
                    <div class="select-arrow-element">
                        <img src="/img/icon/catalog-up-arr.svg" alt="">
                    </div>
                        <div class="select-elemet-item">
                            <div class="checkbox">
                                <label>
                                    <span class="checkelem" >По популярным</span> <input
                                        class="check-box" type="checkbox" wire:model="popular">
                                    <span class='check-style'> </span>
                                </label>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <span class="checkelem" >По cкидкам</span> <input
                                        class="check-box" type="checkbox" wire:model="sales">
                                    <span class='check-style'> </span>
                                </label>
                            </div>

                            {{-- <div class="checkbox">
                                <label>
                                    <span class="checkelem" >{{ $popular }}</span> <input
                                        class="check-box" type="checkbox" wire:model="popular">
                                    <span class='check-style'> </span>
                                </label>

                            </div> --}}
                        </div>
                </div>
            </div>

            <div class="select-items-drop">
                <span class='openselect'>Цена</span> <span></span>
                <div class="select-element-block invisiblewin" id='select-sort'>
                    <div class="select-arrow-element">
                        <img src="/img/icon/catalog-up-arr.svg" alt="">
                    </div>
                        <div class="select-elemet-item">
                            <div class="checkbox">
                                <label>
                                    <span class="checkelem" >По популярным</span> <input
                                        class="check-box" type="checkbox" wire:model="popular">
                                    <span class='check-style'> </span>
                                </label>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <span class="checkelem" >По cкидкам</span> <input
                                        class="check-box" type="checkbox" wire:model="sales">
                                    <span class='check-style'> </span>
                                </label>
                            </div>

                            {{-- <div class="checkbox">
                                <label>
                                    <span class="checkelem" >{{ $popular }}</span> <input
                                        class="check-box" type="checkbox" wire:model="popular">
                                    <span class='check-style'> </span>
                                </label>

                            </div> --}}
                        </div>
                </div>
            </div>

            @foreach ($filters as $filter)
                
                <div class="select-items-drop">
                    <span class='openselect'>{{ $filter->name }}</span> <span></span>
                    <div class="select-element-block invisiblewin" id='select-sort'>
                        <div class="select-arrow-element">
                            <img src="/img/icon/catalog-up-arr.svg" alt="">
                        </div>
                        @foreach ($filter->attributes() as $attr) 
                            <div class="select-elemet-item">
                                <div class="checkbox">
                                    <label>
                                        <span class="checkelem" >{{ $attr->value }}</span> <input
                                            class="check-box" type="checkbox" wire:model="attributes.{{ $attr->value }}">
                                        <span class='check-style'> </span>
                                    </label>

                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            
            @endforeach
        </div>
        <div class="catalog-product">
            @foreach($items as $item)
            <a href="/item/{{$item->id}}" class="body-card-item">
                <div class="card-item">
                    <div class="heard-basket">
                        <img src="/img/card-item/small-heard.png" alt="">
                        <img src="/img/card-item/small-basket.svg" alt="">
                    </div>
                    <div class="things-img">
                        <img src="/storage/{{ $item->image }}" alt="">
                    </div>
                    <div class="name-color">
                        <div class="name-things">{{$item->name}}
                        </div>
                        <div class="things-color">
                            <div class="first-color"></div>
                            <div class="two-color"></div>
                        </div>
                    </div>
                    <div class="type-things">{{ $currentCategory->name }}</div>
                    <div class="things-price-catalog">
                        <div class="price-without-discount-catalog">
                            {{number_format($item->price,0,","," ")}}<span>₸</span></div>
                        <!-- <div class="price-discout"> 20000 <span>₸</span></div> -->
                    </div>
                </div>
            </a>
            @endforeach


        </div>






    </div>
</div>