<div>
<div class="menu-bar-search">
    <div class="bar-modal">
        <div class="input-data">
            <form class="search-new-things" action="">
                <input wire:model="search" tabindex="1" placeholder="Найти товары..." type="text">
            </form>
        </div>
    </div>
</div>
<div class="menu-bar-search-find">
    <div class="bar-search-item">
        @foreach ($items as $item)
            <a href="/item/{{$item->id}}">{{$item->name}}</a>
        @endforeach
    </div>
</div>
</div>