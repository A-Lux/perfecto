@if ($paginator->hasPages())
<div class="pagination-item">
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li><a href="#"><img src="/img/icon/Arrow-pagination-left.png" alt=""></a>
            @else
            <li><a href="{{ $paginator->previousPageUrl() }}"><img src="/img/icon/Arrow-pagination-left.png" alt=""></a>

            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <!-- <li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li> -->
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li><a class="active-page" href="#">{{$page}}</a></li>
                        @else
                            <li><a href="{{$url}}">{{$page}}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li><a href="{{ $paginator->nextPageUrl() }}"><img src="/img/icon/Arrow-pagination-right.png" alt=""></a>
            @else
                <li><a href="#"><img src="/img/icon/Arrow-pagination-right.png" alt=""></a>
            @endif
        </ul>
    </div>
@endif

