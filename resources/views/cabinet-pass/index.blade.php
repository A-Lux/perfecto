@extends('layouts.app')
@push('styles')
<link href="{{ mix('css/cabinet-date.css') }}">
@endpush

@section('content')
<div id="cabinet-date">
    <div class="container px-0">
        <div class="tabs-categories">
            <div class="select-categories pd-cat">
                <ul class="nav" id="myTab" role="tablist">
                    <li class="nav-item " role="presentation">
                        <a class="nav-link active cate-style add-border actives" id="profil-tab" data-toggle="tab" href="#profil"
                            role="tab" aria-controls="profil" aria-selected="true">профиль</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link cate-style add-border" id="order-tab" data-toggle="tab" href="#order" role="tab"
                            aria-controls="order" aria-selected="false">мои заказы</a>
                    </li>
                    <li class="nav-item favorit-top" role="presentation">
                        <a class="nav-link active cate-style add-border" id="favorite-tab" data-toggle="tab" href="#favorite"
                            role="tab" aria-controls="favorite" aria-selected="true">избранное</a>
                    </li>
                    <li class="nav-item padding-top-tabs-container" role="presentation">
                        <a class="nav-link cate-style add-border" id="bonus-tab" data-toggle="tab" href="#bonus" role="tab"
                            aria-controls="bonus" aria-selected="false">МОИ БОНУСЫ</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="profil" role="tabpanel" aria-labelledby="profil-tab">
                    <div class="pass-form-personal-account">
                        <div class="form">
                            <div class="pass-item-personal-account">
                                <div class="left-down-name">
                                    <span><img src="/img/icon/down-left.png" alt=""></span> <span>Изменение
                                        пароля</span>
                                </div>
                                <div class="pass-input-personal-account">
                                    <div class="current-pass-input">
                                        <p>Введите текущий пароль</p>
                                        <input class="view-password" type="password">
                                        <p>Пароль</p>
                                        <span class="show-password"><img src="/img/icon/eyes.png" alt=""></span>
                                    </div>
                                    <div class="forgot-pass-line-personal-account">Забыли пароль?</div>
                                    <div class="new-pass-personal-account">
                                        <p>Введите новый пароль</p>
                                        <input class="view-password" type="password">
                                        <p>Пароль</p>
                                        <span class="show-password"><img src="/img/icon/eyes.png" alt=""></span>
                                    </div>
                                    <div class="repeat-new-pass-personal-account">
                                        <p>Введите новый пароль</p>
                                        <input class="view-password" type="password">
                                        <p>Пароль</p>
                                        <span class="show-password"><img src="/img/icon/eyes.png" alt=""></span>
                                    </div>
                                    <div class="button-group-date-personal">
                                        <button>Отмена</button>
                                        <button data-check>Сохранить</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="order" role="tabpanel" aria-labelledby="order-tab">
                </div>
                <div class="tab-pane fade" id="favorite" role="tabpanel" aria-labelledby="favorite-tab">
                </div>
                <div class="tab-pane fade" id="bonus" role="tabpanel" aria-labelledby="bonus-tab">
                </div>
            </div>
        </div>
    </div>
    <div class="password-update-overlay hide">
        <div class="modal-update-pass">
            <div class="item-update-pass">
                <img src="/img/icon/green-check.png" alt="">
                <p>Ваш пароль бы успешно изменен!</p>
                <button data-check>Хорошо</button>
            </div>
        </div>
    </div>
</div>

@endsection
