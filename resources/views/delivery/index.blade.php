@extends('layouts.app')
@push('styles')
<link href="{{ mix('css/delivery.css') }}">
@endpush

@section('content')
<div id="delivery">
    <div class="container">
        <div class="row">
            <div class="delivery">
                <div class="delivery-title title">Доставка</div>
                <div class="delivery-subtitle subtitle">Способы доставки и оплаты заказов</div>
                <div class="columns">
                    <div class="column">
                        <div class="column-item d-flex">
                            <div class="coll-1-first">
                                <div class="item-mages-first">
                                    <img src="img/icon/car-delivery.png" alt="">
                                </div>
                                <div class="item-title-first">Курьерская доставка
                                    по Нурсултану</div>
                                <div class="item-text-first">Доставка курьером компании AIT осуществляется до любого
                                    адреса в
                                    городе Алматы</div>
                            </div>
                            <div class="coll-2-second">
                                <div class="item-mages-first">
                                    <img src="/img/icon/box-delivery.png" alt="">
                                </div>
                                <div class="item-title-second">Самовывоз
                                    из магазинов</div>
                                <div class="item-text-second">Самовывоз из магазинов в Нур-Султан, Алматы и Актау</div>
                            </div>
                        </div>
                        <div class="column-item">
                            <div class="coll-1 line-top-bot">
                                <div class="item-title pay">Способы оплаты</div>
                                <div class="item-row-column d-flex align-items-center pb-3">
                                    <div class="item-images visa-pay">
                                        <img src="/img/icon/visa-delivery.png" alt="">
                                        <img class="pay-pal" src="/img/icon/pay-pal-delivery.png" alt="">
                                    </div>
                                    <div class="item-text-pay">Наличными при получении</div>
                                </div>

                            </div>
                        </div>
                        <div class="column-item d-flex">
                            <div class="coll-1-first">
                                <div class="item-title-first">Сроки доставки</div>
                                <div class="item-text deliv-pd">от <span class="item-text-bold">1</span> до <span
                                        class="item-text-bold">3</span> рабочих дней</div>
                            </div>
                            <div class="coll-2-second">
                                <div class="item-title-first">Сроки самовывоза</div>
                                <div class="item-text deliv-pd">от <span class="item-text-bold">1</span> до <span
                                        class="item-text-bold">5</span> рабочих дней</div>
                            </div>
                        </div>
                        <div class="column-item">
                            <div class="coll-1 line-top-bot">
                                <div class="item-title-order">Количество товаров в заказе</div>
                                <div class="item-text-order">до <span class="item-text-bold">5</span> товаров</div>
                            </div>
                        </div>
                        <div class="column-item d-flex">
                            <div class="coll-1-first">
                                <div class="item-title-red">Стоимость доставки</div>
                                <div class="item-text">1000 тенге</div>
                            </div>
                            <div class="coll-2-second">
                                <div class="item-title-red">Стоимость доставки</div>
                                <div class="item-text">Бесплатно</div>
                            </div>
                        </div>
                    </div>
                    <div class="column-average">
                        <div class="column-item">
                            <div class="coll-1">
                                <div class="item-mages-earth">
                                    <img src="/img/icon/earth-delivery.png" alt="">
                                </div>
                                <div class="item-title-courier">Курьерская доставка <span class="item-title-red"> По
                                        Казахстану</span></div>
                            </div>
                        </div>
                        <div class="column-item">
                            <div class="coll-1 line-top-bot">
                                <div class="item-title pay">Способы оплаты</div>
                                <div class="item-row-column d-flex align-items-center pb-3">
                                    <div class="item-images visa-pay">
                                        <img src="/img/icon/visa-delivery.png" alt="">
                                        <img class="pay-pal" src="/img/icon/pay-pal-delivery.png" alt="">
                                    </div>
                                    <!-- <div class="item-text-pay">Наличными при получении</div> -->
                                </div>

                            </div>
                        </div>
                        <div class="column-item">
                            <div class="coll-1">
                                <div class="item-title-first">Сроки доставки</div>
                                <div class="item-text-courier deliv-pd">от <span class="item-text-bold">1</span> до
                                    <span class="item-text-bold">3</span> рабочих дней</div>
                            </div>
                            <!-- <div class="coll-2-second">
                                <div class="item-title-first">Сроки самовывоза</div>
                                <div class="item-text deliv-pd">от 1 до 5 рабочих дней</div>
                            </div> -->
                        </div>
                        <div class="column-item">
                            <div class="coll-1 line-top-bot">
                                <div class="item-title-order">Количество товаров в заказе</div>
                                <div class="item-text-order">до <span class="item-text-bold">5</span> товаров</div>
                            </div>
                        </div>
                        <div class="column-item">
                            <div class="coll-1">
                                <div class="item-title-red pd-left">Стоимость доставки</div>
                                <div class="item-text-courier">Уточняется оператором</div>
                            </div>
                            <!-- <div class="coll-2-second">
                                <div class="item-title-red">Стоимость доставки</div>
                                <div class="item-text">Бесплатно</div>
                            </div> -->
                        </div>
                    </div>
                    <div class="column-average mrgt-2rem">
                        <div class="column-item">
                            <div class="coll-1">
                                <div class="item-mages-earth">
                                    <img src="/img/icon/earth-delivery-2.png" alt="">
                                </div>
                                <div class="item-title-courier-world">Курьерская доставка по всему миру</div>
                                <div class="item-text marg-bot">Доставка курьером компании AIT осуществляется до любого
                                    адреса в
                                    городе Алматы</div>
                            </div>
                        </div>
                        <div class="column-item">
                            <div class="coll-1 line-top-bot">
                                <div class="item-title pay">Способы оплаты</div>
                                <div class="item-row-column d-flex align-items-center pb-3">
                                    <div class="item-images visa-pay">
                                        <img src="/img/icon/visa-delivery.png" alt="">
                                        <img class="pay-pal" src="/img/icon/pay-pal-delivery.png" alt="">
                                    </div>
                                    <div class="item-text-pay">Наличными при получении</div>
                                </div>

                            </div>
                        </div>
                        <div class="column-item">
                            <div class="coll-1">
                                <div class="item-title-first">Сроки доставки</div>
                                <div class="item-text-courier deliv-pd">от <span class="item-text-bold">1</span> до
                                    <span class="item-text-bold">3</span> рабочих дней</div>
                            </div>
                            <!-- <div class="coll-2-second">
									  <div class="item-title-first">Сроки самовывоза</div>
									  <div class="item-text deliv-pd">от 1 до 5 рабочих дней</div>
								 </div> -->
                        </div>
                        <div class="column-item">
                            <div class="coll-1 line-top-bot">
                                <div class="item-title-order">Количество товаров в заказе</div>
                                <div class="item-text-order">до <span class="item-text-bold">5</span> товаров</div>
                            </div>
                        </div>
                        <div class="column-item">
                            <div class="coll-1">
                                <div class="item-title-red pd-left">Стоимость доставки</div>
                                <div class="item-text-courier">1000 тенге</div>
                            </div>
                            <!-- <div class="coll-2-second">
									  <div class="item-title-red">Стоимость доставки</div>
									  <div class="item-text">Бесплатно</div>
								 </div> -->
                        </div>
                    </div>
                    <div class="column-courier">
                        <div class="column-item">
                            <div class="coll-1 d-flex flex-column justify-content-center align-items-center">
                                <div class="item-mages-earth">
                                    <img src="/img/icon/car-delivery-2.png" alt="">
                                </div>
                                <div class="item-title-courier-world">Ещё о доставке</div>
                                <div class="item-text-center">Доставка бесплатная
                                    при заказе на сумму <span class="red-price">от 20,000тг</span>по всему Казахстану
                                </div>
                                <div class="item-text marg-bot">
                                    Если необходимо доставить товар по иному адресу, необходимо связаться со службой
                                    поддержки по телефонам: </div>
                            </div>
                        </div>
                        <div class="column-item">
                            <div class="coll-1 line-top-bot">
                                <div class="item-telephone"><img src="/img/icon/phone-delivery.png" alt=""> <span
                                        class="number">+7 702 139-05-51 </span></div>
                            </div>
                        </div>
                        <div class="column-item">
                            <div class="coll-1">
                                <div class="item-telephone"><img src="/img/icon/phone-delivery.png" alt=""> <span
                                        class="number">+7 707 490-59-96 </span></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection
