@extends('layouts.app')
@push('styles')
<link href="{{ mix('css/exchange.css') }}">
@endpush

@section('content')
<div id="exchange">
    <div class="container">
        <div class="row">
            <div class="exchange">
                <div class="exchange-title title">обмен и возврат</div>
                <div class="exchange-subtitle">Возврат приобретенного в интернет-магазине AIT Shoes товара
                    осуществляется согласно Закону Республики Казахстан О защите прав потребителей.</div>
                <div class="exchange-text text">ВНИМАНИЕ! Товар подвергается возврату только при условиях, что обувь не
                    была в употреблении, были сохранены ее товарный вид и потребительские свойства, а также упаковка,
                    фабричные ярлыки и квитанция об оплате либо чек, удостоверяющий покупку.</div>
                <div class="collapsee">
                    <div id="accordion">
                        <div class="card mrg-top bg-F1F1F1">
                            <div class="card-header d-flex justify-content-between align-items-center border-0"
                                id="headingOne">
                                <h5 class="mb-0 collapse-title">
                                    Возврат заказов, доставленных курьером в Алматы
                                </h5>
                                <button class="shadow-none plus-minus" data-toggle="collapse" data-target="#collapseOne"
                                    aria-expanded="true" aria-controls="collapseOne">
                                    <div class="minus-dis"></div>
                                </button>
                            </div>

                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                                data-parent="#accordion">
                                <div class="card-body">
                                    <p>В Алматы действует бесплатная примерка обуви при доставке товара курьером. Вы
                                        можете примерить заказанную обувь и сразу же её вернуть, в случае если она вам
                                        не подошла. Бесплатная примерка доступна в течение 15 минут, по истечении
                                        которых клиент оплачивает 500 тенге за каждые последующие 15 минут.</p>
                                    <p>Для возврата товара свяжитесь с оператором службы поддержки AIT по телефонам: +7
                                        702 139-05-51; +7 707 490-59-96.</p>
                                    <p>Возврат товара осуществляется в магазин AIT или FABIANI by AIT.</p>
                                    <p>Для возврата товара обязательно предоставление:
                                    </p>
                                    <ul>
                                        <li><span>Удостоверения личности;</span></li>
                                        <li><span>Расходной накладной либо чека;</span></li>
                                        <li><span>Заполненного клиентом заявления на возврат товара.</span></li>
                                    </ul>
                                    <p>При возврате заказа, оплаченного курьеру наличными, возврат денег осуществляется
                                        наличными в магазинах AIT/FABIANI by AIT.</p>
                                    <p>При возврате заказа, оплаченного платежной картой онлайн, возврат осуществляется
                                        на платежную карту, откуда была произведена оплата. Возврат денежных средств на
                                        карту клиента осуществляется в течение 3-5 дней.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card mrg-top bg-F1F1F1">
                            <div class="card-header d-flex justify-content-between align-items-center border-0"
                                id="headingTwo">
                                <h5 class="mb-0 collapse-title">
                                    Возврат заказов, доставленных курьером в Нур-Султан
                                </h5>
                                <button class="collapsed shadow-none plus-minus" data-toggle="collapse"
                                    data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <div class="minus-dis"></div>
                                </button>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                data-parent="#accordion">
                                <div class="card-body">
                                    <p>В Алматы действует бесплатная примерка обуви при доставке товара курьером. Вы
                                        можете примерить заказанную обувь и сразу же её вернуть, в случае если она вам
                                        не подошла. Бесплатная примерка доступна в течение 15 минут, по истечении
                                        которых клиент оплачивает 500 тенге за каждые последующие 15 минут.</p>
                                    <p>Для возврата товара свяжитесь с оператором службы поддержки AIT по телефонам: +7
                                        702 139-05-51; +7 707 490-59-96.</p>
                                    <p>Возврат товара осуществляется в магазин AIT или FABIANI by AIT.</p>
                                    <p>Для возврата товара обязательно предоставление:
                                    </p>
                                    <ul>
                                        <li><span>Удостоверения личности;</span></li>
                                        <li><span>Расходной накладной либо чека;</span></li>
                                        <li><span>Заполненного клиентом заявления на возврат товара.</span></li>
                                    </ul>
                                    <p>При возврате заказа, оплаченного курьеру наличными, возврат денег осуществляется
                                        наличными в магазинах AIT/FABIANI by AIT.</p>
                                    <p>При возврате заказа, оплаченного платежной картой онлайн, возврат осуществляется
                                        на платежную карту, откуда была произведена оплата. Возврат денежных средств на
                                        карту клиента осуществляется в течение 3-5 дней.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card mrg-top bg-F1F1F1">
                            <div class="card-header d-flex justify-content-between align-items-center border-0"
                                id="headingThree">
                                <h5 class="mb-0 collapse-title">
                                    Возврат заказов, доставленных курьерской службой в другие города Казахстана

                                </h5>
                                <button class="collapsed shadow-none plus-minus" data-toggle="collapse"
                                    data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <div class="minus-dis"></div>
                                </button>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                data-parent="#accordion">
                                <div class="card-body">
                                    <p>В Алматы действует бесплатная примерка обуви при доставке товара курьером. Вы
                                        можете примерить заказанную обувь и сразу же её вернуть, в случае если она вам
                                        не подошла. Бесплатная примерка доступна в течение 15 минут, по истечении
                                        которых клиент оплачивает 500 тенге за каждые последующие 15 минут.</p>
                                    <p>Для возврата товара свяжитесь с оператором службы поддержки AIT по телефонам: +7
                                        702 139-05-51; +7 707 490-59-96.</p>
                                    <p>Возврат товара осуществляется в магазин AIT или FABIANI by AIT.</p>
                                    <p>Для возврата товара обязательно предоставление:
                                    </p>
                                    <ul>
                                        <li><span>Удостоверения личности;</span></li>
                                        <li><span>Расходной накладной либо чека;</span></li>
                                        <li><span>Заполненного клиентом заявления на возврат товара.</span></li>
                                    </ul>
                                    <p>При возврате заказа, оплаченного курьеру наличными, возврат денег осуществляется
                                        наличными в магазинах AIT/FABIANI by AIT.</p>
                                    <p>При возврате заказа, оплаченного платежной картой онлайн, возврат осуществляется
                                        на платежную карту, откуда была произведена оплата. Возврат денежных средств на
                                        карту клиента осуществляется в течение 3-5 дней.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card mrg-top bg-F1F1F1">
                            <div class="card-header d-flex justify-content-between align-items-center border-0"
                                id="headingFour">
                                <h5 class="mb-0 collapse-title">
                                    Клиент может осуществить возврат товара курьерской службой Exline.
                                </h5>
                                <button class="shadow-none plus-minus" data-toggle="collapse"
                                    data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                    <div class="minus-dis"></div>
                                </button>
                            </div>

                            <div id="collapseFour" class="collapse" aria-labelledby="headingFour"
                                data-parent="#accordion">
                                <div class="card-body">
                                    <p>В Алматы действует бесплатная примерка обуви при доставке товара курьером. Вы
                                        можете примерить заказанную обувь и сразу же её вернуть, в случае если она вам
                                        не подошла. Бесплатная примерка доступна в течение 15 минут, по истечении
                                        которых клиент оплачивает 500 тенге за каждые последующие 15 минут.</p>
                                    <p>Для возврата товара свяжитесь с оператором службы поддержки AIT по телефонам: +7
                                        702 139-05-51; +7 707 490-59-96.</p>
                                    <p>Возврат товара осуществляется в магазин AIT или FABIANI by AIT.</p>
                                    <p>Для возврата товара обязательно предоставление:
                                    </p>
                                    <ul>
                                        <li><span>Удостоверения личности;</span></li>
                                        <li><span>Расходной накладной либо чека;</span></li>
                                        <li><span>Заполненного клиентом заявления на возврат товара.</span></li>
                                    </ul>
                                    <p>При возврате заказа, оплаченного курьеру наличными, возврат денег осуществляется
                                        наличными в магазинах AIT/FABIANI by AIT.</p>
                                    <p>При возврате заказа, оплаченного платежной картой онлайн, возврат осуществляется
                                        на платежную карту, откуда была произведена оплата. Возврат денежных средств на
                                        карту клиента осуществляется в течение 3-5 дней.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card mrg-top bg-F1F1F1">
                            <div class="card-header d-flex justify-content-between align-items-center border-0"
                                id="headingFive">
                                <h5 class="mb-0 collapse-title">
                                    Возможна передача товара в отделение КазПочта (Стоимость тарифов отправки посылок
                                    КазПочтой уточняйте в отделениях или на сайте post.kz ).
                                </h5>
                                <button class="shadow-none plus-minus" data-toggle="collapse"
                                    data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                                    <div class="minus-dis"></div>
                                </button>
                            </div>

                            <div id="collapseFive" class="collapse" aria-labelledby="headingFive"
                                data-parent="#accordion">
                                <div class="card-body">
                                    <p>В Алматы действует бесплатная примерка обуви при доставке товара курьером. Вы
                                        можете примерить заказанную обувь и сразу же её вернуть, в случае если она вам
                                        не подошла. Бесплатная примерка доступна в течение 15 минут, по истечении
                                        которых клиент оплачивает 500 тенге за каждые последующие 15 минут.</p>
                                    <p>Для возврата товара свяжитесь с оператором службы поддержки AIT по телефонам: +7
                                        702 139-05-51; +7 707 490-59-96.</p>
                                    <p>Возврат товара осуществляется в магазин AIT или FABIANI by AIT.</p>
                                    <p>Для возврата товара обязательно предоставление:
                                    </p>
                                    <ul>
                                        <li><span>Удостоверения личности;</span></li>
                                        <li><span>Расходной накладной либо чека;</span></li>
                                        <li><span>Заполненного клиентом заявления на возврат товара.</span></li>
                                    </ul>
                                    <p>При возврате заказа, оплаченного курьеру наличными, возврат денег осуществляется
                                        наличными в магазинах AIT/FABIANI by AIT.</p>
                                    <p>При возврате заказа, оплаченного платежной картой онлайн, возврат осуществляется
                                        на платежную карту, откуда была произведена оплата. Возврат денежных средств на
                                        карту клиента осуществляется в течение 3-5 дней.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="exchange-contact">
                    <p>Адрес получателя при возврате:</p>
                    <p>Казахстан, г. Алматы, ул. Манаса 9б, офис 205</p>
                    <p>+7 702 139-05-51; +7 707 490-59-96</p>
                </div>

            </div>
        </div>
    </div>
</div>
<script src="{{ mix('js/collapse.js') }}"></script>

@endsection
