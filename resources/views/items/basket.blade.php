@extends('layouts.app')
@push('styles')
<link href="{{ mix('css/basket.css') }}">
@endpush

@section('content')
<div id="basket">
    <div class="container px-0">
        <div class="basket-main-block">
            <div class="basket-block-things">
                <div class="basket-title-things">Моя корзина<span> 3 товара</span></div>
                <div class="basket-things-table">
                    <div class="basket-type-things-item basket-border-bottom-line">
                        <div class="picture-basket-item">
                            <img src="/img/pictures/tyfli.png" alt="">
                        </div>
                        <div class="description-basket-item">
                            <div class="basket-item-card">
                                <div class="name-basket">BOTTEGA VENETA
                                    <p>Сандали</p>
                                </div>
                                <div class="close-basket-btn">
                                    <img src="/img/pictures/Close.png" alt="">
                                </div>
                            </div>
                            <div class="basket-size-color-price">
                                <div class="basket-size">
                                    <p>Размер</p>
                                    <div class="item-size-basket">
                                        38
                                    </div>
                                </div>
                                <div class="basket-color">
                                    <p>Цвет</p>
                                    <div class="colors-item-type-things">
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                    </div>
                                </div>
                                <div class="basket-price">
                                    <p>5 000</p> <span> ₸</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="basket-type-things-item basket-border-bottom-line">
                        <div class="picture-basket-item">
                            <img src="/img/pictures/crosi.png" alt="">
                        </div>
                        <div class="description-basket-item">
                            <div class="basket-item-card">
                                <div class="name-basket">BOTTEGA VENETA
                                    <p>Сандали</p>
                                </div>
                                <div class="close-basket-btn">
                                    <img src="/img/pictures/Close.png" alt="">
                                </div>
                            </div>
                            <div class="basket-size-color-price">
                                <div class="basket-size">
                                    <p>Размер</p>
                                    <div class="item-size-basket">
                                        38
                                    </div>
                                </div>
                                <div class="basket-color">
                                    <p>Цвет</p>
                                    <div class="colors-item-type-things">
                                        <div></div>
                                        <div></div>
                                    </div>
                                </div>
                                <div class="basket-price">
                                    <p>5 000</p> <span> ₸</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="basket-type-things-item">
                        <div class="picture-basket-item">
                            <img src="/img/pictures/symka.png" alt="">
                        </div>
                        <div class="description-basket-item">
                            <div class="basket-item-card">
                                <div class="name-basket">BOTTEGA VENETA
                                    <p>Сандали</p>
                                </div>
                                <div class="close-basket-btn">
                                    <img src="/img/pictures/Close.png" alt="">
                                </div>
                            </div>
                            <div class="basket-size-color-price">
                                <div class="basket-size">
                                    <p>Размер</p>
                                    <div class="item-size-basket">
                                        38
                                    </div>
                                </div>
                                <div class="basket-color">
                                    <p>Цвет</p>
                                    <div class="colors-item-type-things">
                                        <div></div>
                                        <div></div>
                                    </div>
                                </div>
                                <div class="basket-price">
                                    <p>5 000</p> <span> ₸</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="description-prices-all-things">
                    <div class="left-part-price">
                        <div class="things-on-summa"> Товаров на сумму
                            <p>19 000<span>₸</span></p>
                        </div>
                        <div class="delivery-summa"> Стоимость доставки
                            <p>1 000<span>₸</span></p>
                        </div>
                        <div class="bonus-summa"> Использовано бонусов
                            <p>1536<span>₸</span></p>
                        </div>
                    </div>
                    <div class="right-part-price">
                        <div class="total-for-summa"> Итого к оплате
                            <p>18 464<span>₸</span></p>
                        </div>
                    </div>

                </div>
            </div>
            <div class="basket-block-form">
                <div class="basket-title-form">Оформление заказа</div>
                <div class="basket-form-order">
                    <form action="">
                        <div class="horizontal-input-form-order">
                            <label class="basket-main-name">
                                <input tabindex="1" type="text">
                                <p>Имя</p>
                            </label>
                            <label class="basket-main-surname">
                                <input tabindex="2" type="text">
                                <p>Фамилия</p>
                            </label>
                        </div>
                        <div class="horizontal-input-form-order">
                            <label class="basket-main-email">
                                <input tabindex="3" type="email">
                                <p>E-mail</p>
                            </label>
                            <label class="basket-main-phone">
                                <input tabindex="4" type="tel">
                                <p>Телефон</p>
                            </label>
                        </div>
                        <div class="form-order-horizontal-basket">
                            <div class="form-title-basket">Выберите способ доставки</div>
                            <div class="form-subtitle-basket">Подроднее о доставке</div>
                        </div>
                        <div class="tabs-basket-form">
                            <ul class="nav nav-pills mb-3 basket-navs" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active " id="pills-home-tab" data-toggle="pill"
                                        href="#pills-home" role="tab" aria-controls="pills-home"
                                        aria-selected="true">Курьером</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile"
                                        role="tab" aria-controls="pills-profile" aria-selected="false">Курьером
                                        Exline</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact"
                                        role="tab" aria-controls="pills-contact" aria-selected="false">Самовывоз</a>
                                </li>
                            </ul>
                            <div class="tab-content tab-content-basket" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                                    aria-labelledby="pills-home-tab">
                                    <div class="attention-basket-tabs">
                                        <span><img src="/img/icon/danger.png" alt=""></span><span>от 20 000 тг доставка
                                            по РК осуществляется бесплатно</span>
                                    </div>
                                    <div class="basket-tabs-addres">
                                        <div class="tabs-addres-title">Укажите ваш адрес</div>
                                        <label class="basket-main-addres">
                                            <input tabindex="5" type="text">
                                            <p>Местоположение</p>
                                        </label>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pills-profile" role="tabpanel"
                                    aria-labelledby="pills-profile-tab">...</div>
                                <div class="tab-pane fade" id="pills-contact" role="tabpanel"
                                    aria-labelledby="pills-contact-tab">
                                    <div class="take-market-basket-item">
                                        <div class="market-title">
                                            Выбор магазина для самовывоза
                                        </div>
                                        <div class="market-box-block">
                                            <span class="toggle-item-shop"></span>
                                            <div class="market-box-item market-bottom-line">
                                                <p>Арена</p>
                                                <p>ТРЦ MEGA Alma-Ata (ул. Розыбакиева 247A), бутик FABIANI</p>
                                                <p>Пн.-Вс. с 10:00 до 22:00</p>
                                            </div>
                                            <div class="market-box-item market-bottom-line checkshop invisiblewin">
                                                <p>MEGA Park</p>
                                                <p>ТРЦ MEGA PARK (ул. Макатаева 127), бутик FABIANI</p>
                                                <p>Пн.-Вс. с 10:00 до 22:00</p>
                                            </div>
                                            <div class="market-box-item market-bottom-line checkshop invisiblewin">
                                                <p>Арена</p>
                                                <p>ТД Арена (ул. Абая уг. ул. Гагарина), бутик AIT No.73</p>
                                                <p>Пн.-Сб. с 10:00 до 20:00 Вс. с 10:00 до 19:00</p>
                                            </div>
                                            <div class="market-box-item market-bottom-line checkshop invisiblewin">
                                                <p>Байтурсынова 106</p>
                                                <p>ул. Байтурсынова 106 (уг. ул. Сатпаева), салон обуви AIT</p>
                                                <p>Пн.-Сб. с 10:00 до 20:00 Вс. с 10:00 до 19:00</p>
                                            </div>
                                            <div class="market-box-item checkshop invisiblewin">
                                                <p>Керемет</p>
                                                <p>Мкр-н Керемет 7, корпус 31, салон обуви AIT</p>
                                                <p>Пн.-Сб. с 10:00 до 21:00 Вс. с 10:00 до 20:00</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="basket-bonus-system">
                            <div class="bonus-title">Потратить бонусы</div>
                            <div class="spent-bonuses">
                                <div class="accumulated-bonus">
                                    <p>Накоплено<span>1536</span>Б</p>
                                    <p>( Лимит использования до 50 % от заказа)</p>
                                </div>
                                <div class="toggle-bonus">
                                    <label class="switch">
                                        <input type="checkbox" id="toggle-switch">
                                        <span class="toggle-switch"></span>
                                        <input type="hidden" class="toggle-value">
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="take-type-pay">
                            <div class="title-type-pay">Выберите способ оплаты</div>
                            <div class="take-type-btn-group">
                                <label class="basket-main-online">
                                    <input checked type="checkbox" id="paycard" class="clickcheck payonline" value="1">
                                    <span class="circl-checkbox"></span>
                                    <p class="paystatus">Онлайн-оплата платежной картой</p>
                                    <input class="basket-online" type="hidden">

                                </label>
                                <label class="basket-main-cash">
                                    <input type="checkbox" id="paycard" class="clickcheck  paycash" value="2">
                                    <span class="circl-checkbox"></span>
                                    <p class="paystatus checkedstatus">Оплата наличными</p>
                                </label>
                            </div>
                        </div>
                        <div class="checkout-btn">
                            <button>Оформить заказ</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection
