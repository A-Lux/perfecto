@extends('layouts.app')
@push('styles')
<link href="{{ mix('css/card-things.css') }}">
@endpush

@section('content')
<div id="card-things">
    <div class="container px-0">
        <div class="tabs-categories">
            <div class="select-categories pd-cat">
                <ul class="nav" id="myTab" role="tablist">
                    <li class="nav-item " role="presentation">
                        <a class="nav-link active cate-style add-border actives" id="home-tab" data-toggle="tab"
                            href="#home" role="tab" aria-controls="home" aria-selected="true">ЖЕНЩИНАМ</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link cate-style add-border" id="profile-tab" data-toggle="tab" href="#profile"
                            role="tab" aria-controls="profile" aria-selected="false">МУЖЧИНАМ</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="main-card-shell">
                        <div class="left-shell">
                            <div class="main-card-images">
                                <div class="bread-crumb">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item crumb-item-href"><a href="/catalog/all">Каталог</a>
                                            </li>
                                            {{-- <li class="breadcrumb-item crumb-item-href"><a href="#">женщинам</a></li> --}}
                                            <li class="breadcrumb-item crumb-item-href"><a href="/catalog/{{ App\Subcategory::find($item->category_id)->id }}">{{ App\Subcategory::find($item->category_id)->name }}</a></li>
                                            <li class="breadcrumb-item crumb-item-href active" aria-current="page">
                                                {{ substr($item->name,0,20) . '...' }}
                                            </li>
                                        </ol>
                                    </nav>
                                </div>
                                <div class="card-things-images">
                                    <div class="swiper-container gallery-top">

                                       
                                        <div class="swiper-wrapper">

                                            @foreach(json_decode($item->images, true) as $image)
                                                <div class="swiper-slide card-thing-slides"> <img
                                                        src="/storage/{{ $image }}" alt="">
                                                </div>
                                            @endforeach
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="main-small-images">
                                <div class="swiper-container gallery-thumbs">
                                    <div class="swiper-wrapper">

                                            @foreach(json_decode($item->images, true) as $image)
                                                <div class="swiper-slide">
                                                    <div class="images-things">
                                                        <img src="/storage/{{ $image }}" alt="">
                                                    </div>
                                                </div>
                                            @endforeach
                                            
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="right-shell">
                            <div class="tabs-type-things">
                                <div class="select-categories pd-cat cat-st">
                                    <ul class="nav" id="myTab" role="tablist">
                                        <li class="nav-item " role="presentation">
                                            <a class="nav-link active cate-style add-border actives" id="wnthings-tab"
                                                data-toggle="tab" href="#wnthings" role="tab" aria-controls="mnthings"
                                                aria-selected="true">обзор товара</a>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link cate-style add-border" id="mnthings-tab"
                                                data-toggle="tab" href="#mnthings" role="tab" aria-controls="mnthings"
                                                aria-selected="false">описание</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="wnthings" role="tabpanel"
                                        aria-labelledby="wnthings-tab">
                                        <div class="main-name-things-card">{{$item->name}}</div>
                                        <div class="main-text-things-card">В наличии</div>
                                        <div class="main-table-things-card">Размер <span data-open>таблица
                                                размеров</span></div>


                                        <div class="main-select-table-size-shoes">
                                            <div class="take-size-main-shoes">
                                                <span>Выбрать размер</span><span id="dropDownArrow"><img
                                                        src="/img/icon/arrow.png" alt=""></span>
                                            </div>
                                            <div class="take-size-shoes-card hide">

                                                @foreach(explode(',',$item->sizes) as $size)
                                                <div class="more-size-card-things"><span>37</span></div>
                                                @endforeach

                                            </div>

                                            <div class="main-price-things-card">
                                                <span>42500 тг.</span> <span>{{number_format($item->price,0,","," ")}}
                                                    тг.</span>
                                                <p>5% от стоимости заказа</p>
                                                <p>250 бонусов</p>
                                            </div>
                                        </div>

                                        <div class="down-part-card">
                                            <div class="link-card-things-basket">
                                                <a href="#">Добавить в корзину</a>
                                            </div>
                                            <div class="favorite-card-things">
                                                <img src="/img/icon/red-heard-card.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="mnthings" role="tabpanel"
                                        aria-labelledby="mnthings-tab">
                                        <div class="main-name-things-card">{{$item->name}}</div>
                                        <div class="paragraph-things-card">
                                            <p>{!!$item->description!!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    </div>
                </div>
            </div>
            <div class="hit-season">
                <div class="summer-goods">
                    <div class="goods-title">Популярное</div>
                    <div class="goods-cards">
                        <a href="#" class="body-card-item">
                            <div class="card-item">
                                <div class="heard-basket">
                                    <img src="/img/card-item/small-heard.png" alt="">
                                    <img src="/img/card-item/small-basket.svg" alt="">
                                </div>
                                <div class="things-img">
                                    <img src="/img/card-item/shoes.png" alt="">
                                </div>
                                <div class="name-color">
                                    <div class="name-things">BOTTEGA VENETA
                                    </div>
                                    <div class="things-color">
                                        <div class="first-color"></div>
                                        <div class="two-color"></div>
                                    </div>
                                </div>
                                <div class="type-things">Мокасины</div>
                                <div class="things-price">
                                    <div class="price-without-discount"> 41 000 <span>₸</span></div>
                                    <div class="price-discout"> 20000 <span>₸</span></div>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="body-card-item">
                            <div class="card-item">
                                <div class="heard-basket">
                                    <img src="/img/card-item/small-heard.png" alt="">
                                    <img src="/img/card-item/small-basket.svg" alt="">
                                </div>
                                <div class="things-img">
                                    <img src="/img/card-item/shoes.png" alt="">
                                </div>
                                <div class="name-color">
                                    <div class="name-things">BOTTEGA VENETA
                                    </div>
                                    <div class="things-color">
                                        <div class="first-color"></div>
                                        <div class="two-color"></div>
                                    </div>
                                </div>
                                <div class="type-things">Мокасины</div>
                                <div class="things-price">
                                    <div class="price-without-discount"> 41 000 <span>₸</span></div>
                                    <div class="price-discout"> 20000 <span>₸</span></div>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="body-card-item">
                            <div class="card-item">
                                <div class="heard-basket">
                                    <img src="/img/card-item/small-heard.png" alt="">
                                    <img src="/img/card-item/small-basket.svg" alt="">
                                </div>
                                <div class="things-img">
                                    <img src="/img/card-item/shoes.png" alt="">
                                </div>
                                <div class="name-color">
                                    <div class="name-things">BOTTEGA VENETA
                                    </div>
                                    <div class="things-color">
                                        <div class="first-color"></div>
                                        <div class="two-color"></div>
                                    </div>
                                </div>
                                <div class="type-things">Мокасины</div>
                                <div class="things-price">
                                    <div class="price-without-discount"> 41 000 <span>₸</span></div>
                                    <div class="price-discout"> 20000 <span>₸</span></div>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="body-card-item">
                            <div class="card-item">
                                <div class="heard-basket">
                                    <img src="/img/card-item/small-heard.png" alt="">
                                    <img src="/img/card-item/small-basket.svg" alt="">
                                </div>
                                <div class="things-img">
                                    <img src="/img/card-item/shoes.png" alt="">
                                </div>
                                <div class="name-color">
                                    <div class="name-things">BOTTEGA VENETA
                                    </div>
                                    <div class="things-color">
                                        <div class="first-color"></div>
                                        <div class="two-color"></div>
                                    </div>
                                </div>
                                <div class="type-things">Мокасины</div>
                                <div class="things-price">
                                    <div class="price-without-discount"> 41 000 <span>₸</span></div>
                                    <div class="price-discout"> 20000 <span>₸</span></div>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="body-card-item">
                            <div class="card-item">
                                <div class="heard-basket">
                                    <img src="/img/card-item/small-heard.png" alt="">
                                    <img src="/img/card-item/small-basket.svg" alt="">
                                </div>
                                <div class="things-img">
                                    <img src="/img/card-item/shoes.png" alt="">
                                </div>
                                <div class="name-color">
                                    <div class="name-things">BOTTEGA VENETA
                                    </div>
                                    <div class="things-color">
                                        <div class="first-color"></div>
                                        <div class="two-color"></div>
                                    </div>
                                </div>
                                <div class="type-things">Мокасины</div>
                                <div class="things-price">
                                    <div class="price-without-discount"> 41 000 <span>₸</span></div>
                                    <div class="price-discout"> 20000 <span>₸</span></div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="modal-overlay-card hide">
        <div class="full-table-card">
            <div class="table-size-card ">
                <div class="table-size-name">
                    <div class="table-title">
                        ЖЕнская обувь
                    </div>
                    <div class="table-size-close">
                        <button data-exit type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                <div class="table-line-size">
                    <div class="row-table-size-card">
                        <span>RU</span>
                        <span>EURO</span>
                        <span>Длина стопы, см</span>
                    </div>
                </div>
                <div class="table-line-size">
                    <div class="row-table-size-card-center">
                        <div class="item-table-size">37</div>
                        <div class="item-table-size">37</div>
                        <div class="item-table-size">25,5</div>
                    </div>
                </div>
                <div class="table-line-size">
                    <div class="row-table-size-card-center">
                        <div class="item-table-size">38</div>
                        <div class="item-table-size">38</div>
                        <div class="item-table-size">25,5</div>
                    </div>
                </div>
                <div class="table-line-size">
                    <div class="row-table-size-card-center">
                        <div class="item-table-size">39</div>
                        <div class="item-table-size">39</div>
                        <div class="item-table-size">25,5</div>
                    </div>
                </div>
                <div class="table-line-size">
                    <div class="row-table-size-card-center">
                        <div class="item-table-size">40</div>
                        <div class="item-table-size">40</div>
                        <div class="item-table-size">25,5</div>
                    </div>
                </div>
                <div class="table-line-size">
                    <div class="row-table-size-card-center">
                        <div class="item-table-size">41</div>
                        <div class="item-table-size">41</div>
                        <div class="item-table-size">25,5</div>
                    </div>
                </div>
                <div class="table-line-size">
                    <div class="row-table-size-card-center">
                        <div class="item-table-size">42</div>
                        <div class="item-table-size">42</div>
                        <div class="item-table-size">25,5</div>
                    </div>
                </div>
                <div class="table-line-size">
                    <div class="row-table-size-card-center">
                        <div class="item-table-size">43</div>
                        <div class="item-table-size">43</div>
                        <div class="item-table-size">25,5</div>
                    </div>
                </div>
                <div class="table-line-size">
                    <div class="row-table-size-card-center">
                        <div class="item-table-size">44</div>
                        <div class="item-table-size">44</div>
                        <div class="item-table-size">25,5</div>
                    </div>
                </div>
                <div class="table-line-size">
                    <div class="row-table-size-card-center">
                        <div class="item-table-size">45</div>
                        <div class="item-table-size">45</div>
                        <div class="item-table-size">25,5</div>
                    </div>
                </div>
                <div class="table-line-size border-0">
                    <div class="row-table-size-card-center">
                        <div class="item-table-size">46</div>
                        <div class="item-table-size">46</div>
                        <div class="item-table-size">25,5</div>
                    </div>
                </div>

                <div class="table-our-size">
                    <div class="our-size-title">Как определить свой размер</div>
                    <div class="our-block-size">
                        <div class="paragraph-our-size">
                            <p>Вам понадобится провести измерения с помощью сантиметровой ленты.</p>
                            <p>Поставьте ногу на чистый лист бумаги. Отметьте крайние границы ступни и измерьте
                                расстояние
                                между самыми удаленными точками стопы.</p>
                        </div>
                        <div class="images-our-size">
                            <img src="/img/icon/leg-size.png" alt="">
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <script src="{{ mix('js/type-thing.js') }}"></script>



    @endsection
