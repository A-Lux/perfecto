@extends('layouts.app')
@push('styles')
<link href="{{ mix('css/action.css') }}">
@endpush

@section('content')
<div id="action">
    <div class="container px-0">
        <div class="action-title title">Акции</div>
        <div class="tabs-action">
            <div class="select-categories select-action">
                <ul class="nav tab-action size-full" id="myTab" role="tablist">
                    <li class="nav-item item-woman-bg-nav item-click-type" role="presentation">
                        <a class="nav-link active style-action-woman action-item-type action-click" id="home-tab" data-toggle="tab" href="#home"
                            role="tab" aria-controls="home" aria-selected="true"><div class=" click-type click-border ">ЖЕНЩИНАМ</div></a>
                    </li>
                    <li class="nav-item item-men-bg-nav item-click-type" role="presentation">
                        <a class="nav-link style-action-men action-item-type" id="profile-tab" data-toggle="tab" href="#profile"
                            role="tab" aria-controls="profile" aria-selected="false"><div class="click-type">МУЖЧИНАМ</div></a>
                    </li>
                </ul>
            </div>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <!-- <div class="types-action">
                    </div> -->
                    <div class="swiper-container container-slider-action">
                        <div class="swiper-wrapper ">
                            <div class="swiper-slide slider-categories">
                                <div class="action-column">
                                    <div class="action-item-images">
                                        <img src="/img/pictures/shoes-nike.png" alt="">
                                    </div>
                                    <div class="action-item-text">
                                        <div class="text-sale-action">-75%</div>
                                        <div class="text-title-action">Best cross for sport lead</div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide slider-categories">
                                <div class="action-column">
                                    <div class="action-item-images">
                                        <img src="/img/pictures/shoes-nike.png" alt="">

                                    </div>
                                    <div class="action-item-text">
                                        <div class="text-sale-action">-75%</div>
                                        <div class="text-title-action">Best cross for sport lead</div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide slider-categories">
                                <div class="action-column">
                                    <div class="action-item-images">
                                        <img src="/img/pictures/shoes-nike.png" alt="">
                                    </div>
                                    <div class="action-item-text">
                                        <div class="text-sale-action">-75%</div>
                                        <div class="text-title-action">Best cross for sport lead</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-button-prev prev-arrow"></div>
                        <div class="swiper-button-next next-arrow"></div>
                    </div>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                </div>
            </div>
        </div>
        <div class="actions-goods">
            <div class="action-main-block">
                <div class="action-title-shoes">Cкидки по женской обуви</div>
                <div class="action-views-all"><a href="">показать все</a></div>
            </div>
            <div class="goods-cards">                        @foreach ($items as $item)
                <a href="/item/{{$item->id}}" class="body-card-item">
                    <div class="card-item">
                        <div class="heard-basket">
                            <img src="/img/card-item/small-heard.png" alt="">
                            <img src="/img/card-item/small-basket.svg" alt="">
                        </div>
                        <div class="things-img">
                            <img src="/storage/{{ $item->image }}" alt="">
                        </div>
                        <div class="name-color">
                            <div class="name-things">{{$item->name}}
                            </div>
                            <div class="things-color">
                                <div class="first-color"></div>
                                <div class="two-color"></div>
                            </div>
                        </div>
                        <div class="type-things"></div>
                        <div class="things-price">
                            <div class="price-without-discount"> 41 000 <span>₸</span></div>
                            <div class="price-discout"> 20000 <span>₸</span></div>
                        </div>
                    </div>
                </a>
            @endforeach
            </div>
            <!-- <div class="goods-button">
                <button class="view-more">Смотреть еще</button>
            </div> -->
        </div>
    </div>
</div>

@endsection
