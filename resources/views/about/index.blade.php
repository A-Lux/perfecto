@extends('layouts.app')
@push('styles')
<link href="{{ mix('css/about.css') }}">
@endpush

@section('content')
<div id="about">
    <div class="container px-0">
        <div class="about-title title">о нас</div>
        <div class="about-description">
            <div class="description-about-text">Компания "Alfa Delivery Services" основанная в апреле 2006 года является
                официальным бизнес-партнером всемирно известной компании "UPS" в Нур-султан и авторизованным
                сервис-партнероми в городах Казахстана: Нур-султан, Кокшетау, Актау, Жанаозен, Баутино, Атырау, Уральск,
                Аксай, Петропавловск, Актобе, Хромтау, Тенгиз, Павлодар, Усть-Каменогорск, Караганда, Кызылорда.
                <p>Компания "Alfa Delivery Services" имеет возможность предложить вам широкий спектр услуг
                    экспресс-доставки, по принципу "от двери до двери", в области перевозок корреспонденции и грузов.
                </p>
            </div>
            <div class="description-about-images">
                <img src="/img/pictures/shoes-nike.png" alt="">
            </div>
        </div>
    </div>
</div>

@endsection
