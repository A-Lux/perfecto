@extends('layouts.app')
@push('styles')
<link href="{{ mix('css/home.css') }}">
@endpush

@section('content')
<div id="home">
    <div class="slider">
        <div class="swiper-container container-slider-main-page">
            <div class="swiper-wrapper">
                @foreach ($slider as $slide)
                    
                    <div class="swiper-slide">
                        <div class="slid-pictures">
                            <a href="{{ $slide->link }}">
                                <img src="/storage/{{ $slide->image }}" alt="">
                            </a>
                        </div>
                    </div>
                    
                @endforeach

            </div>
            <div class="swiper-pagination"></div>
            <!-- <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div> -->
        </div>

    </div>
    <div class="products">
        <div class="container">
            <div class="types">
                <a href="/catalog/11">
                <div class="woman">
                    <div class="woman-title">Женское</div>
                    <div class="woman-arrow">
                        <img src="/img/icon/arrow-right.svg" alt="">
                    </div>
                </div>
                </a>
                <a href="/catalog/11">
                    <div class="men">
                        <div class="men-title">Мужское</div>
                        <div class="men-arrow">
                            <img src="/img/icon/arrow-right.svg" alt="">
                        </div>
                    </div>
                </a>
            </div>
            <div class="types-og-goods">
                <div class="summer-goods">
                    <div class="goods-title">Летние новинки Perfectto</div>
                    <div class="goods-cards">
                        @foreach ($new as $item)   
                    <a href="/item/{{$item->id}}" class="body-card-item">
                            <div class="card-item">
                                <div class="heard-basket">
                                    <img src="/img/card-item/small-heard.png" alt="">
                                    <img src="/img/card-item/small-basket.svg" alt="">
                                </div>
                                <div class="things-img">
                                    <img src="{{$item->image}}" alt="">
                                </div>
                                <div class="name-color">
                                    <div class="name-things">{{$item->name}}
                                    </div>
                                    <div class="things-color">
                                        <div class="first-color"></div>
                                        <div class="two-color"></div>
                                    </div>
                                </div>
                                <div class="type-things">{{App\Subcategory::find($item->category_id)->first()->name}}</div>
                                <div class="things-price">
                                    <div class="price-without-discount"> 41 000 <span>₸</span></div>
                                    <div class="price-discout"> 20000 <span>₸</span></div>
                                </div>
                            </div>
                        </a>
                        @endforeach

                       
                    </div>
                    <div class="goods-button">
                        <button class="view-more">Смотреть еще</button>
                    </div>
                </div>
                <div class="summer-goods">
                    <div class="goods-title">Популярные аксессуары</div>
                    <div class="goods-cards">
                        @foreach ($popular as $item)
                            <a href="/item/{{$item->id}}" class="body-card-item">
                                <div class="card-item">
                                    <div class="heard-basket">
                                        <img src="/img/card-item/small-heard.png" alt="">
                                        <img src="/img/card-item/small-basket.svg" alt="">
                                    </div>
                                    <div class="things-img">
                                        <img src="{{$item->image}}" alt="">
                                    </div>
                                    <div class="name-color">
                                        <div class="name-things">{{$item->name}}
                                        </div>
                                        <div class="things-color">
                                            <div class="first-color"></div>
                                            <div class="two-color"></div>
                                        </div>
                                    </div>
                                    <div class="type-things">{{App\Subcategory::find($item->category_id)->first()->name}}</div>
                                    <div class="things-price">
                                        <div class="price-without-discount"> 41 000 <span>₸</span></div>
                                        <div class="price-discout"> 20000 <span>₸</span></div>
                                    </div>
                                </div>
                            </a>
                        @endforeach
                        
                    </div>
                    <div class="goods-button">
                        <button class="view-more">Смотреть еще</button>
                    </div>
                </div>
                <div class="summer-goods">
                    <div class="goods-title">Актуальное со скидкой</div>
                    <div class="goods-cards">
                        @foreach ($onSale as $item)
                        <a href="/item/{{$item->id}}" class="body-card-item">
                            <div class="card-item">
                                <div class="heard-basket">
                                    <img src="/img/card-item/small-heard.png" alt="">
                                    <img src="/img/card-item/small-basket.svg" alt="">
                                </div>
                                <div class="things-img">
                                    <img src="{{$item->image}}" alt="">
                                </div>
                                <div class="name-color">
                                    <div class="name-things">{{$item->name}}
                                    </div>
                                    <div class="things-color">
                                        <div class="first-color"></div>
                                        <div class="two-color"></div>
                                    </div>
                                </div>
                                <div class="type-things">{{App\Subcategory::find($item->category_id)->first()->name}}</div>
                                <div class="things-price">
                                    <div class="price-without-discount"> 41 000 <span>₸</span></div>
                                    <div class="price-discout"> 20000 <span>₸</span></div>
                                </div>
                            </div>
                        </a>
                    @endforeach
                    </div>
                    <div class="goods-button">
                        <button class="view-more">Смотреть еще</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="articles">
        <div class="container">
            <div class="column-articles">
                <div class="column-name">
                    <div class="last-article">Последние статьи</div>
                    <div class="all-article">Все статьи <a href="/posts"><span><img src="/img/icon/red-right-arrow.svg" alt=""></span></a>
                    </div>
                </div>
                <div class="card-articles">
                    @foreach ($posts as $post)
                    <a href="/post/{{$post->id}}">
                        <div class="articles-item border-line-bottom">
                        <div class="article-item-card-title">{{$post->name}}</div>
                            <div class="article-item-card-subtitle">{{$post->description}}</div>
                        </div>
                    </a>
                    @endforeach

                    
                </div>
            </div>
        </div>

    </div>

</div>


@endsection
