@extends('layouts.app')
@push('styles')
<link href="{{ mix('css/cabinet.css') }}">
@endpush

@section('content')
<div id="cabinet">
    <div class="container px-0">
        <div class="tabs-categories">
            <div class="select-categories pd-cat">
                <ul class="nav nav-tabs brd-none" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active cate-style add-border actives" id="profile-tab" data-toggle="tab"
                            href="#profile" role="tab" aria-controls="profile" aria-selected="true">профиль</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link cate-style add-border" id="order-tab" data-toggle="tab" href="#order"
                            role="tab" aria-controls="order" aria-selected="false">мои заказы</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link cate-style add-border" id="favorite-tab" data-toggle="tab" href="#favorite"
                            role="tab" aria-controls="favorite" aria-selected="false">избранное</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link cate-style add-border" id="bonus-tab" data-toggle="tab" href="#bonus"
                            role="tab" aria-controls="bonus" aria-selected="false">МОИ БОНУСЫ</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="personal-account">
                        <div class="personal-form">
                            <form action="">
                                <div class="account-item-personal">
                                    <div class="item-name-close">
                                        <div class="form-title-account">Личный кабинет</div>
                                        <div class="form-btn-close-personal">
                                            <button>Выйти</button>
                                        </div>
                                    </div>
                                    <div class="item-input-account-name">
                                        <input tabindex="1" type="text">
                                        <p>Фамилия</p>
                                        <input tabindex="2" type="text">
                                        <p>Имя</p>
                                        <input tabindex="3" type="tel">
                                        <p>Телефон</p>
                                        <input class="date-input-personal-area dpnone" tabindex="4" type="text">
                                    </div>
                                    <div class="item-input-account-date-pass">
                                        <input class="date-input-personal-area dpnonebig" tabindex="4" type="text">
                                        <p>Дата рождения</p>
                                        <span><img src="/img/icon/calendar.png" alt=""></span>
                                        <input class="pass-input-personal-area" tabindex="5" type="password">
                                        <span><img src="/img/icon/pen.png" alt=""></span>
                                        <p>Пароль</p>
                                    </div>
                                    <div class="item-input-account-email">
                                        <input tabindex="6" type="email">
                                        <p>E-mail</p>
                                    </div>
                                    <div class="item-submit-button-account">
                                        <button tabindex="7" type="submit">Сохранить данные</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="order" role="tabpanel" aria-labelledby="order-tab">
                    <div class="my-orders">
                        <div class="my-order-block">
                            <div class="item-order-title">
                                <span>Мои заказы</span> <span>12 товаров</span>
                            </div>
                            <div class="my-order-block-item">
                                <div class="item-order-card-line">
                                    <div class="card-line-order-text-part">
                                        <p>№ KZ200555-6460252</p>
                                        <p>Доставлен 18 мая</p>
                                        <p>Сумма к оплате 23 500 тг.</p>
                                        <p>Подробнее</p>
                                    </div>
                                    <div class="card-line-order-picture-part">
                                        <span><img src="/img/pictures/ultra-small-tapki.png" alt=""></span>
                                        <span><img src="/img/pictures/ultra-small-tapki.png" alt=""></span>
                                        <span><img src="/img/pictures/ultra-small-tapki.png" alt=""></span>
                                    </div>
                                </div>
                            </div>
                            <div class="my-order-block-item">
                                <div class="item-order-card-line">
                                    <div class="card-line-order-text-part">
                                        <p>№ KZ200555-6460252</p>
                                        <p>Доставлен 18 мая</p>
                                        <p>Сумма к оплате 23 500 тг.</p>
                                        <p>Подробнее</p>
                                    </div>
                                    <div class="card-line-order-picture-part">
                                        <span><img src="/img/pictures/ultra-small-tapki.png" alt=""></span>
                                        <span><img src="/img/pictures/ultra-small-tapki.png" alt=""></span>
                                        <span class="dpnonebig"><img src="/img/pictures/ultra-small-tapki.png"
                                                alt=""></span>
                                        <span><img src="/img/pictures/plus_2.png" alt=""></span>
                                    </div>
                                </div>
                            </div>
                            <div class="my-order-block-item padding-my-order-block-item">
                                <div class="item-order-card-line">
                                    <div class="card-line-order-text-part">
                                        <p>№ KZ200555-6460252</p>
                                        <p>Доставлен 18 мая</p>
                                        <p>Сумма к оплате 23 500 тг.</p>
                                        <p>Подробнее</p>
                                    </div>
                                    <div class="card-line-order-picture-part">
                                        <span><img src="/img/pictures/ultra-small-tapki.png" alt=""></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="favorite" role="tabpanel" aria-labelledby="favorite-tab">
                    <div class="favorite-block">
                        <div class="favorite-item-block">
                            <div class="favorite-item-type">
                                <div class="favorit-select-type-item">
                                    <div class="item-main-favorit"><span><img src="/img/icon/white-heart.png"
                                                alt=""></span><span>Избранное</span></div>
                                    <div class="item-second-favorite active-type favorite-dp-none">Обувь</div>
                                    <div class="item-second-favorite favorite-dp-none">Мокасины</div>
                                    <div data-input class="item-second-favorite active-type favorite-dp-block">Все
                                        товары
                                        <span><img src="/img/icon/arrow.png" alt=""></span></div>
                                </div>
                            </div>
                            <div class="favorite-item-things">
                                <div class="goods-cards">
                                    <a href="#" class="body-card-item">
                                        <div class="card-item">
                                            <div class="heard-basket">
                                                <img src="/img/card-item/small-heard.png" alt="">
                                                <img src="/img/card-item/small-basket.svg" alt="">
                                            </div>
                                            <div class="things-img">
                                                <img src="/img/card-item/watch.png" alt="">
                                            </div>
                                            <div class="name-color">
                                                <div class="name-things">BOTTEGA VENETA
                                                </div>
                                                <div class="things-color">
                                                    <div class="first-color"></div>
                                                    <div class="two-color"></div>
                                                </div>
                                            </div>
                                            <div class="type-things">Мокасины</div>
                                            <div class="things-price">
                                                <div class="price-without-discount"> 41 000 <span>₸</span></div>
                                                <div class="price-discout"> 20000 <span>₸</span></div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="body-card-item">
                                        <div class="card-item">
                                            <div class="heard-basket">
                                                <img src="/img/card-item/small-heard.png" alt="">
                                                <img src="/img/card-item/small-basket.svg" alt="">
                                            </div>
                                            <div class="things-img">
                                                <img src="/img/card-item/watch.png" alt="">
                                            </div>
                                            <div class="name-color">
                                                <div class="name-things">BOTTEGA VENETA
                                                </div>
                                                <div class="things-color">
                                                    <div class="first-color"></div>
                                                    <div class="two-color"></div>
                                                </div>
                                            </div>
                                            <div class="type-things">Мокасины</div>
                                            <div class="things-price">
                                                <div class="price-without-discount"> 41 000 <span>₸</span></div>
                                                <div class="price-discout"> 20000 <span>₸</span></div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="body-card-item">
                                        <div class="card-item">
                                            <div class="heard-basket">
                                                <img src="/img/card-item/small-heard.png" alt="">
                                                <img src="/img/card-item/small-basket.svg" alt="">
                                            </div>
                                            <div class="things-img">
                                                <img src="/img/card-item/watch.png" alt="">
                                            </div>
                                            <div class="name-color">
                                                <div class="name-things">BOTTEGA VENETA
                                                </div>
                                                <div class="things-color">
                                                    <div class="first-color"></div>
                                                    <div class="two-color"></div>
                                                </div>
                                            </div>
                                            <div class="type-things">Мокасины</div>
                                            <div class="things-price">
                                                <div class="price-without-discount"> 41 000 <span>₸</span></div>
                                                <div class="price-discout"> 20000 <span>₸</span></div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="body-card-item">
                                        <div class="card-item">
                                            <div class="heard-basket">
                                                <img src="/img/card-item/small-heard.png" alt="">
                                                <img src="/img/card-item/small-basket.svg" alt="">
                                            </div>
                                            <div class="things-img">
                                                <img src="/img/card-item/watch.png" alt="">
                                            </div>
                                            <div class="name-color">
                                                <div class="name-things">BOTTEGA VENETA
                                                </div>
                                                <div class="things-color">
                                                    <div class="first-color"></div>
                                                    <div class="two-color"></div>
                                                </div>
                                            </div>
                                            <div class="type-things">Мокасины</div>
                                            <div class="things-price">
                                                <div class="price-without-discount"> 41 000 <span>₸</span></div>
                                                <div class="price-discout"> 20000 <span>₸</span></div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="body-card-item">
                                        <div class="card-item">
                                            <div class="heard-basket">
                                                <img src="/img/card-item/small-heard.png" alt="">
                                                <img src="/img/card-item/small-basket.svg" alt="">
                                            </div>
                                            <div class="things-img">
                                                <img src="/img/card-item/watch.png" alt="">
                                            </div>
                                            <div class="name-color">
                                                <div class="name-things">BOTTEGA VENETA
                                                </div>
                                                <div class="things-color">
                                                    <div class="first-color"></div>
                                                    <div class="two-color"></div>
                                                </div>
                                            </div>
                                            <div class="type-things">Мокасины</div>
                                            <div class="things-price">
                                                <div class="price-without-discount"> 41 000 <span>₸</span></div>
                                                <div class="price-discout"> 20000 <span>₸</span></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="bonus" role="tabpanel" aria-labelledby="bonus-tab">
                    <div class="bonus-pc-title">Бонусная система</div>
                    <div class="bonus-system-cabinet">
                        <div class="bonus-view-pc">
                            <div class="bonus-pc-subtitle">Как она работает</div>
                            <div class="bonus-pc-text">При покупке товара на любую сумму вы получаете 5 % бонуса от
                                заказа независимо от цены самого товара</div>
                            <div class="bonus-pc-images">
                                <img src="/img/pictures/pc-bonus.png" alt="">
                                <p>На сайте</p>
                            </div>
                            <div class="bonus-pc-description">
                                <p>Если вы захотите потратить ваши бонусы то у вас списываются все бонусы которые вы
                                    накопили</p>
                                <p>Вы можете использовать максимум 50% бонусов от стоимости товара. При покупке с
                                    использованием бонусов там сумма за которую вы оплатили начисляются бонусы</p>
                            </div>
                        </div>
                        <div class="bonus-view-mob">
                            <div class="bonus-mob-images">
                                <img src="/img/pictures/mob-bonus.png" alt="">
                                <p>Мобильное приложение</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="overlay-for-all-favorite hide">
        <div class="favorite-form-all-things">
            <form action="">
                <div class="item-all-favorite"><label for="1"><span>Мокасины</span><input data-input id="1"
                            type="radio"></div>
                </label>
                <div class="item-all-favorite"><label for="2"><span>Туфли</span><input data-input id="2" type="radio">
                </div>
                </label>
            </form>
        </div>
    </div>
</div>

@endsection
