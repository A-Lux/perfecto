@extends('layouts.app')
@push('styles')
<link href="{{ mix('css/cabinet-date.css') }}">
@endpush

@section('content')
<div id="cabinet-date">
    <div class="container px-0">
        <div class="tabs-categories">
            <div class="select-categories pd-cat">
                <ul class="nav" id="myTab" role="tablist">
                    <li class="nav-item " role="presentation">
                        <a class="nav-link active cate-style add-border actives" id="profil-tab" data-toggle="tab" href="#profil"
                            role="tab" aria-controls="profil" aria-selected="true">профиль</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link cate-style add-border" id="order-tab" data-toggle="tab" href="#order" role="tab"
                            aria-controls="order" aria-selected="false">мои заказы</a>
                    </li>
                    <li class="nav-item favorit-top" role="presentation">
                        <a class="nav-link active cate-style add-border" id="favorite-tab" data-toggle="tab" href="#favorite"
                            role="tab" aria-controls="favorite" aria-selected="true">избранное</a>
                    </li>
                    <li class="nav-item padding-top-tabs-container" role="presentation">
                        <a class="nav-link cate-style add-border" id="bonus-tab" data-toggle="tab" href="#bonus" role="tab"
                            aria-controls="bonus" aria-selected="false">МОИ БОНУСЫ</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="profil" role="tabpanel" aria-labelledby="profil-tab">
                    <div class="date-form-account-personal">
                        <form action="">
                            <div class="date-item-personal-account">
                                <div class="left-down-name">
                                    <span><img src="/img/icon/down-left.png" alt=""></span> <span>Дата рождения</span>
                                </div>
                                <div class="date-inputs-block">
                                    <input tabindex="1" type="text">
                                    <p>День</p>
                                    <select tabindex="2">
                                        <option value="mou-1">Январь</option>
                                        <option value="mou-2">Февраль</option>
                                        <option value="mou-3">Март</option>
                                        <option value="mou-4">Апрель</option>
                                        <option value="mou-5">Май</option>
                                        <option value="mou-6">Июнь</option>
                                        <option value="mou-7">Июль</option>
                                        <option value="mou-8">Август</option>
                                        <option value="mou-9">Сентябрь</option>
                                        <option value="mou-10">Октябрь</option>
                                        <option value="mou-11">Ноябрь</option>
                                        <option value="mou-12">Декабрь</option>
                                    </select>
                                    <p>Месяц</p>
                                    <input tabindex="3" type="text">
                                    <p>Год</p>
                                </div>
                                <div class="button-group-date-personal">
                                    <button>Отмена</button>
                                    <button>Сохранить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane fade" id="order" role="tabpanel" aria-labelledby="order-tab">
                    <div class="my-order-card-block">
                        <div class="my-order-card-inform">
                            <div class="order-card-bread-crumb">
                                <div class="bread-crumb">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item crumb-item-href"><a href="#">Мои заказы</a>
                                            </li>
                                            <li class="breadcrumb-item crumb-item-href"><a href="#">Информация о
                                                    заказе</a></li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                            <div class="order-card-name">
                                Информация о заказе
                            </div>
                            <div class="two-block-order">
                                <div class="order-item-card-block">
                                    <p>№ KZ200555-6460252</p>
                                    <p>Доставлен 18 мая</p>
                                    <p>Продавец</p>
                                    <p>Perfectto</p>
                                    <p>Получатель</p>
                                    <p>Марина Иванова</p>
                                    <p>+7 777 254 64 25</p>
                                    <p>marina_ivanova@gmail.com</p>
                                    <p>Адрес доставки</p>
                                    <p>Алматы, Курмангазы 124А</p>
                                </div>
                                <div class="order-item-card-block-down">
                                    <p>Оплата</p>
                                    <p><span>Товаров в заказе</span><span>3</span></p>
                                    <p><span>Товар на сумму</span><span>24 550 тг.</span></p>
                                    <p><span>Доставка</span><span>1700 тг.</span></p>
                                    <p><span>Скидка</span><span>200 тг.</span></p>
                                    <p><span>Вы экономите</span><span>200 тг.</span></p>
                                    <p><span>Итого</span><span>26 450 тг.</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="my-order-card-pictures">
                            <div class="my-order-card-name-pictures">Товары в заказе</div>
                            <div class="type-card-in-block">
                                <div class="small-card-item-order">
                                    <div class="small-card-images">
                                        <img src="/img/pictures/ultra-small-tapki.png" alt="">
                                    </div>
                                    <div class="small-card-text-inform">
                                        <p>Обувь-Saisai</p>
                                        <p>Размер: 36</p>
                                        <p>Цвет: черный</p>
                                        <p><span>24550</span> <span>24550</span></p>
                                    </div>
                                </div>
                                <div class="small-card-item-order small-card-item-order-pd-top">
                                    <div class="small-card-images">
                                        <img src="/img/pictures/ultra-small-tapki.png" alt="">
                                    </div>
                                    <div class="small-card-text-inform">
                                        <p>Обувь-Saisai</p>
                                        <p>Размер: 36</p>
                                        <p>Цвет: черный</p>
                                        <p><span>24550</span> <span>24550</span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="type-card-in-block">
                                <div class="small-card-item-order">
                                    <div class="small-card-images">
                                        <img src="/img/pictures/ultra-small-tapki.png" alt="">
                                    </div>
                                    <div class="small-card-text-inform">
                                        <p>Обувь-Saisai</p>
                                        <p>Размер: 36</p>
                                        <p>Цвет: черный</p>
                                        <p><span>24550</span> <span>24550</span></p>
                                    </div>
                                </div>
                                <div class="small-card-item-order small-card-item-order-pd-top">
                                    <div class="small-card-images">
                                        <img src="/img/pictures/ultra-small-tapki.png" alt="">
                                    </div>
                                    <div class="small-card-text-inform">
                                        <p>Обувь-Saisai</p>
                                        <p>Размер: 36</p>
                                        <p>Цвет: черный</p>
                                        <p><span>24550</span> <span>24550</span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="type-card-in-block">
                                <div class="small-card-item-order">
                                    <div class="small-card-images">
                                        <img src="/img/pictures/ultra-small-tapki.png" alt="">
                                    </div>
                                    <div class="small-card-text-inform">
                                        <p>Обувь-Saisai</p>
                                        <p>Размер: 36</p>
                                        <p>Цвет: черный</p>
                                        <p><span>24550</span> <span>24550</span></p>
                                    </div>
                                </div>
                                <div class="small-card-item-order small-card-item-order-pd-top">
                                    <div class="small-card-images">
                                        <img src="/img/pictures/ultra-small-tapki.png" alt="">
                                    </div>
                                    <div class="small-card-text-inform">
                                        <p>Обувь-Saisai</p>
                                        <p>Размер: 36</p>
                                        <p>Цвет: черный</p>
                                        <p><span>24550</span> <span>24550</span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="type-card-in-block">
                                <div class="small-card-item-order">
                                    <div class="small-card-images">
                                        <img src="/img/pictures/ultra-small-tapki.png" alt="">
                                    </div>
                                    <div class="small-card-text-inform">
                                        <p>Обувь-Saisai</p>
                                        <p>Размер: 36</p>
                                        <p>Цвет: черный</p>
                                        <p><span>24550</span> <span>24550</span></p>
                                    </div>
                                </div>
                                <div class="small-card-item-order small-card-item-order-pd-top">
                                    <div class="small-card-images">
                                        <img src="/img/pictures/ultra-small-tapki.png" alt="">
                                    </div>
                                    <div class="small-card-text-inform">
                                        <p>Обувь-Saisai</p>
                                        <p>Размер: 36</p>
                                        <p>Цвет: черный</p>
                                        <p><span>24550</span> <span>24550</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="favorite" role="tabpanel" aria-labelledby="favorite-tab">
                    <div class="favorite-text-attention">
                        Чтобы добавлять товар в избранное <span>зарегистрируйтесь</span> или <span>войдите на
                            сайт</span>
                    </div>
                </div>
                <div class="tab-pane fade" id="bonus" role="tabpanel" aria-labelledby="bonus-tab">

                </div>
            </div>
        </div>
    </div>


</div>

@endsection
