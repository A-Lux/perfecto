@extends('layouts.app')
@push('styles')
<link href="{{ mix('css/catalog.css') }}">
@endpush

@section('content')
<div id="catalog">
    <div class="container px-0">
        <div class="tabs-categories">
            <div class="select-categories pd-cat">
                <ul class="nav" id="myTab" role="tablist">
                    <li class="nav-item " role="presentation">
                        <a class="nav-link active cate-style add-border actives" id="woman-tab" data-toggle="tab"
                            href="#woman" role="tab" aria-controls="woman" aria-selected="true">ЖЕНЩИНАМ</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link cate-style add-border" id="men-tab" data-toggle="tab" href="#men" role="tab"
                            aria-controls="men" aria-selected="false">МУЖЧИНАМ</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="woman" role="tabpanel" aria-labelledby="woman-tab">
                    <div class="catalog-main-block">
                        <div class="catalog-main-name-title">Женская обувь<span>{{App\Item::all()->count()}}
                                товаров</span></div>
                        <div class="catalog-main-filter-item">
                            <div class="catalog-filter-type-thing">
                                @foreach($categories as $category)

                                <div
                                    class="filter-type-thing-name @if($currentCategory->category_id == $category->id) active-filter-type @endif">
                                    @if($loop->index == 1)
                                    Аксессуары
                                    @else
                                    Обувь
                                    @endif
                                    <span data-viewer><img src="/img/icon/small-drop.png" alt=""></span></div>
                                <div class="filter-type-things-item">

                                    @foreach($category->subcategories as $cat)
                                    <a href="/catalog/{{$cat->id}}" @if($currentCategory->id == $cat->id)
                                        style="color:red" @endif>{{$cat->name}}</a>
                                    @endforeach

                                    <!-- <a href="#">Сандалии</a>
                                    <a href="#">Босоножки</a>
                                    <a href="#">Ботильоны</a>
                                    <a href="#">Ботинки</a>
                                    <a href="#">Кроссовки</a>
                                    <a href="#">Обувь на плоской подошве</a>
                                    <a href="#">Сапоги</a>
                                    <a href="#">Слиперы и тапочки</a>
                                    <a href="#">Туфли</a> -->
                                </div>
                                @endforeach

                            </div>
                            @livewire('item-sort', ['currentCategory' => $currentCategory])
                        </div>
                        <div class="entry-pagination">
                            {{ $items->links() }}

                        </div>
                        <div class="catalog-goods">
                            <div class="goods-title">Популярное</div>
                            <div class="goods-cards">
                                <a href="#" class="body-card-item">
                                    <div class="card-item">
                                        <div class="heard-basket">
                                            <img src="/img/card-item/small-heard.png" alt="">
                                            <img src="/img/card-item/small-basket.svg" alt="">
                                        </div>
                                        <div class="things-img">
                                            <img src="/img/card-item/pink-tapki.png" alt="">
                                        </div>
                                        <div class="name-color">
                                            <div class="name-things">BOTTEGA VENETA
                                            </div>
                                            <div class="things-color">
                                                <div class="first-color"></div>
                                                <div class="two-color"></div>
                                            </div>
                                        </div>
                                        <div class="type-things">Мокасины</div>
                                        <div class="things-price-catalog">
                                            <div class="price-without-discount-catalog"> 10 000 <span>₸</span></div>
                                            <!-- <div class="price-discout"> 20000 <span>₸</span></div> -->
                                        </div>
                                    </div>
                                </a>
                                <a href="#" class="body-card-item">
                                    <div class="card-item">
                                        <div class="heard-basket">
                                            <img src="/img/card-item/small-heard.png" alt="">
                                            <img src="/img/card-item/small-basket.svg" alt="">
                                        </div>
                                        <div class="things-img">
                                            <img src="/img/card-item/pink-tapki.png" alt="">
                                        </div>
                                        <div class="name-color">
                                            <div class="name-things">BOTTEGA VENETA
                                            </div>
                                            <div class="things-color">
                                                <div class="first-color"></div>
                                                <div class="two-color"></div>
                                            </div>
                                        </div>
                                        <div class="type-things">Мокасины</div>
                                        <div class="things-price-catalog">
                                            <div class="price-without-discount-catalog"> 10 000 <span>₸</span></div>
                                            <!-- <div class="price-discout"> 20000 <span>₸</span></div> -->
                                        </div>
                                    </div>
                                </a>
                                <a href="#" class="body-card-item">
                                    <div class="card-item">
                                        <div class="heard-basket">
                                            <img src="/img/card-item/small-heard.png" alt="">
                                            <img src="/img/card-item/small-basket.svg" alt="">
                                        </div>
                                        <div class="things-img">
                                            <img src="/img/card-item/pink-tapki.png" alt="">
                                        </div>
                                        <div class="name-color">
                                            <div class="name-things">BOTTEGA VENETA
                                            </div>
                                            <div class="things-color">
                                                <div class="first-color"></div>
                                                <div class="two-color"></div>
                                            </div>
                                        </div>
                                        <div class="type-things">Мокасины</div>
                                        <div class="things-price-catalog">
                                            <div class="price-without-discount-catalog"> 10 000 <span>₸</span></div>
                                            <!-- <div class="price-discout"> 20000 <span>₸</span></div> -->
                                        </div>
                                    </div>
                                </a>
                                <a href="#" class="body-card-item">
                                    <div class="card-item">
                                        <div class="heard-basket">
                                            <img src="/img/card-item/small-heard.png" alt="">
                                            <img src="/img/card-item/small-basket.svg" alt="">
                                        </div>
                                        <div class="things-img">
                                            <img src="/img/card-item/pink-tapki.png" alt="">
                                        </div>
                                        <div class="name-color">
                                            <div class="name-things">BOTTEGA VENETA
                                            </div>
                                            <div class="things-color">
                                                <div class="first-color"></div>
                                                <div class="two-color"></div>
                                            </div>
                                        </div>
                                        <div class="type-things">Мокасины</div>
                                        <div class="things-price-catalog">
                                            <div class="price-without-discount-catalog"> 10 000 <span>₸</span></div>
                                            <!-- <div class="price-discout"> 20000 <span>₸</span></div> -->
                                        </div>
                                    </div>
                                </a>
                                <a href="#" class="body-card-item">
                                    <div class="card-item">
                                        <div class="heard-basket">
                                            <img src="/img/card-item/small-heard.png" alt="">
                                            <img src="/img/card-item/small-basket.svg" alt="">
                                        </div>
                                        <div class="things-img">
                                            <img src="/img/card-item/pink-tapki.png" alt="">
                                        </div>
                                        <div class="name-color">
                                            <div class="name-things">BOTTEGA VENETA
                                            </div>
                                            <div class="things-color">
                                                <div class="first-color"></div>
                                                <div class="two-color"></div>
                                            </div>
                                        </div>
                                        <div class="type-things">Мокасины</div>
                                        <div class="things-price-catalog">
                                            <div class="price-without-discount-catalog"> 10 000 <span>₸</span></div>
                                            <!-- <div class="price-discout"> 20000 <span>₸</span></div> -->
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="men" role="tabpanel" aria-labelledby="men-tab">
                </div>
            </div>
        </div>
    </div>
    <div class="catalog-modal hide" data-catalog>
        <div class="catalog-modal-item">
            <form action="">
                <div data-hide class="type-thing-catalog-item">
                    <label for="10">Сандалии
                        <input class='check-box' type="checkbox" name="" id="10">
                        <span class='check-style check-style-pos'> </span>
                    </label>
                </div>
                <div data-hide class="type-thing-catalog-item">
                    <label for="11">Босоножки
                        <input class='check-box' type="checkbox" name="" id="11">
                        <span class='check-style check-style-pos'> </span>
                    </label>
                </div>
                <div data-hide class="type-thing-catalog-item">
                    <label for="12">Ботильоны
                        <input class='check-box' type="checkbox" name="" id="12">
                        <span class='check-style check-style-pos'> </span>
                    </label>
                </div>
                <div data-hide class="type-thing-catalog-item">
                    <label for="13">Ботинки
                        <input class='check-box' type="checkbox" name="" id="13">
                        <span class='check-style check-style-pos'> </span>
                    </label>
                </div>
                <div data-hide class="type-thing-catalog-item">
                    <label for="14">Кроссовки
                        <input class='check-box' type="checkbox" name="" id="14">
                        <span class='check-style check-style-pos'> </span>
                    </label>
                </div>
                <div data-hide class="type-thing-catalog-item">
                    <label for="15">Обувь на плоской подошве
                        <input class='check-box' type="checkbox" name="" id="15">
                        <span class='check-style check-style-pos'> </span>
                    </label>
                </div>
                <div data-hide class="type-thing-catalog-item">
                    <label for="16">Сапоги
                        <input class='check-box' type="checkbox" name="" id="16">
                        <span class='check-style check-style-pos'> </span>
                    </label>
                </div>
                <div data-hide class="type-thing-catalog-item">
                    <label for="17">Слиперы и тапочки
                        <input class='check-box' type="checkbox" name="" id="17">
                        <span class='check-style check-style-pos'> </span>
                    </label>
                </div>
                <div data-hide class="type-thing-catalog-item">
                    <label for="18">Туфли
                        <input class='check-box' type="checkbox" name="" id="18">
                        <span class='check-style check-style-pos'> </span>
                    </label>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="{{ mix('js/catalog-viewer.js') }}"></script>

@endsection
