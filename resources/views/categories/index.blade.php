@extends('layouts.app')
@push('styles')
<link href="{{ mix('css/categories.css') }}">
@endpush

@section('content')
<div id="categories">
    <div class="container px-0">
        <div class="tabs-categories">
            <div class="select-categories pd-cat">
                <ul class="nav" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active cate-style add-border actives" id="home-tab" data-toggle="tab" href="#home" role="tab"
                            aria-controls="home" aria-selected="true">ЖЕНЩИНАМ</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link cate-style add-border" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                            aria-controls="profile" aria-selected="false">МУЖЧИНАМ</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="type-categories">
                        <div class="shoes-categories">
                            <img src="/img/pictures/shoes-categories.png" alt="">
                            <div class="description-categories">
                                <div class="description-title">Обувь</div>
                            </div>
                        </div>
                        <div class="accessories-categories">
                            <img src="/img/pictures/accesories-categories.png" alt="">
                            <div class="description-categories">
                                <div class="description-title">Аксессуары</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="type-categories">
                        <div class="shoes-categories">
                            <img src="/img/pictures/shoes-categories.png" alt="">
                            <div class="description-categories">
                                <div class="description-title">Мокасины</div>
                            </div>
                        </div>
                        <div class="accessories-categories">
                            <img src="/img/pictures/accesories-categories.png" alt="">
                            <div class="description-categories">
                                <div class="description-title">Сумки</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hit-season">
            <div class="summer-goods">
                <div class="goods-title">Хит сезона</div>
                <div class="goods-cards">
                    <div class="card-item">
                        <div class="heard-basket">
                            <img src="/img/card-item/small-heard.png" alt="">
                            <img src="/img/card-item/small-basket.svg" alt="">
                        </div>
                        <div class="things-img">
                            <img src="/img/card-item/shoes.png" alt="">
                        </div>
                        <div class="name-color">
                            <div class="name-things">BOTTEGA VENETA
                            </div>
                            <div class="things-color">
                                <div class="first-color"></div>
                                <div class="two-color"></div>
                            </div>
                        </div>
                        <div class="type-things">Мокасины</div>
                        <div class="things-price">
                            <div class="price-without-discount"> 41 000 <span>₸</span></div>
                            <div class="price-discout"> 20000 <span>₸</span></div>
                        </div>
                    </div>
                    <div class="card-item">
                        <div class="heard-basket">
                            <img src="/img/card-item/small-heard.png" alt="">
                            <img src="/img/card-item/small-basket.svg" alt="">
                        </div>
                        <div class="things-img">
                            <img src="/img/card-item/shoes.png" alt="">
                        </div>
                        <div class="name-color">
                            <div class="name-things">BOTTEGA VENETA
                            </div>
                            <div class="things-color">
                                <div class="first-color"></div>
                                <div class="two-color"></div>
                            </div>
                        </div>
                        <div class="type-things">Мокасины</div>
                        <div class="things-price">
                            <div class="price-without-discount"> 41 000 <span>₸</span></div>
                            <div class="price-discout"> 20000 <span>₸</span></div>
                        </div>
                    </div>
                    <div class="card-item">
                        <div class="heard-basket">
                            <img src="/img/card-item/small-heard.png" alt="">
                            <img src="/img/card-item/small-basket.svg" alt="">
                        </div>
                        <div class="things-img">
                            <img src="/img/card-item/shoes.png" alt="">
                        </div>
                        <div class="name-color">
                            <div class="name-things">BOTTEGA VENETA
                            </div>
                            <div class="things-color">
                                <div class="first-color"></div>
                                <div class="two-color"></div>
                            </div>
                        </div>
                        <div class="type-things">Мокасины</div>
                        <div class="things-price">
                            <div class="price-without-discount"> 41 000 <span>₸</span></div>
                            <div class="price-discout"> 20000 <span>₸</span></div>
                        </div>
                    </div>
                    <div class="card-item">
                        <div class="heard-basket">
                            <img src="/img/card-item/small-heard.png" alt="">
                            <img src="/img/card-item/small-basket.svg" alt="">
                        </div>
                        <div class="things-img">
                            <img src="/img/card-item/shoes.png" alt="">
                        </div>
                        <div class="name-color">
                            <div class="name-things">BOTTEGA VENETA
                            </div>
                            <div class="things-color">
                                <div class="first-color"></div>
                                <div class="two-color"></div>
                            </div>
                        </div>
                        <div class="type-things">Мокасины</div>
                        <div class="things-price">
                            <div class="price-without-discount"> 41 000 <span>₸</span></div>
                            <div class="price-discout"> 20000 <span>₸</span></div>
                        </div>
                    </div>
                    <div class="card-item">
                        <div class="heard-basket">
                            <img src="/img/card-item/small-heard.png" alt="">
                            <img src="/img/card-item/small-basket.svg" alt="">
                        </div>
                        <div class="things-img">
                            <img src="/img/card-item/shoes.png" alt="">
                        </div>
                        <div class="name-color">
                            <div class="name-things">BOTTEGA VENETA
                            </div>
                            <div class="things-color">
                                <div class="first-color"></div>
                                <div class="two-color"></div>
                            </div>
                        </div>
                        <div class="type-things">Мокасины</div>
                        <div class="things-price">
                            <div class="price-without-discount"> 41 000 <span>₸</span></div>
                            <div class="price-discout"> 20000 <span>₸</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>



@endsection
