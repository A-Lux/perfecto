@extends('layouts.app')
@push('styles')
<link href="{{ mix('css/entry.css') }}">
@endpush

@section('content')
<div id="entry">
    <div class="container px-0">
        <div class="entry-title title">Статьи</div>
        <div class="entry-row">

        @foreach($posts as $post)
            <div class="entry-column">
                <a href="/post/{{$post->id}}">
                    <div class="item-entry-column">
                        <div class="entry-images">
                            <img src="/storage/{{ $post->cover_image }}" alt="">
                        </div>
                        <div class="name-entry">{{$post->name}}</div>
                        <div class="text-entry">{{$post->bigname}}</div>
                        <div class="statistics-entry">
                            <div class="views-entry">
                                <img src="/img/icon/views-eyes.svg" alt="">
                                <span>{{ $post->views }}</span>
                            </div>
                            <div class="date-entry">
                                <span class="entry-mounth">{{ date('d-m-Y', strtotime($post->created_at)) }}</span>
                            </div>
                        </div>
                    </div>
                </a>

            </div>
        @endforeach

      
         
        </div>
        <div class="entry-pagination">
            <div class="pagination-item">
                <ul>
                    <li><a href="#"><img src="/img/icon/Arrow-pagination-left.png" alt=""></a></li>
                    <li><a href="#">1</a></li>
                    <li><a class="active" href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">.....</a></li>
                    <li><a href="#">15</a></li>
                    <li><a href="#"><img src="/img/icon/Arrow-pagination-right.png" alt=""></a></li>
                </ul>
            </div>
        </div>
    </div>

</div>

@endsection
