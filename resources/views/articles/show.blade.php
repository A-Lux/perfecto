@extends('layouts.app')
@push('styles')
<link href="{{ mix('css/paper.css') }}">
@endpush

@section('content')
<div id="paper">
    <div class="container px-0">
        <div class="paper-main-picture">
            <img src="/storage/{{ $post->main_image }}" alt="">
        </div>
        <div class="main-container">
            <div class="paper-main-name">
                <div class="paper-title">{{$post->name}}</div>
                <div class="paper-statistic">
                    <div class="date-paper">
                        <span class="entry-mounth">{{date('d-m-Y', strtotime($post->created_at)) }}</span>
                    </div>
                    <div class="views-paper">
                        <img src="/img/icon/views-eyes.svg" alt="">
                        <span>{{ $post->views }}</span>
                    </div>
                </div>
            </div>
            <!-- <div class="paper-subtitle">Темы: крепкая обувь</div> -->
            <!-- <div class="paper-text text-style-paper d-flex flex-column align-items-start">
                <p>Секретарь выполняет ряд функций, тем самым экономит время руководителя, предоставляя ему краткий
                    обзор
                    важной корреспонденции, статей, книг, научных работ.</p>
            </div>
            <div class="paper-text text-style-paper d-flex flex-column align-items-end">
                <p>Эксперты журнала «Делопроизводство в Казахстане» составили рекомендации для кадровых служб о том,
                    какими
                    навыками должен обладать секретарь-референт.</p>
            </div> -->
            <div class="paper-text text-style-paper d-flex flex-column align-items-start">
                <p>                <p>{{$post->text}}</p>
</p>
            </div>
            <div class="paper-center-picture">
                <img src="/storage/{{ $post->second_image }}" alt="">
            </div>
            <!-- <div class="paper-text text-style-paper d-flex flex-column align-items-end">
                <p>Секретарь имеет дело не только с руководителем, но и с его подчинёнными, а также с людьми из других
                    организаций и очень важно уметь поддержать разговор, где-то дистанцироваться. Секретарь должен
                    обладать
                    мудростью, чтобы понимать, что всё, что он слышит, с чем работает, конфиденциально и доверяется ему
                    не в
                    целях распространения. Иными словами, умение молчать – это большой плюс для секретаря.</p>
            </div>
            <div class="paper-text text-style-paper d-flex flex-column align-items-start">
                <p>Многие успешные бизнесвумен начинали свою карьеру именно с должности секретаря. Где, как не на этой
                    должности, можно было увидеть «изнутри» работу огромного механизма под названием «компания»,
                    научиться
                    коммуницировать, работать в режиме многозадачности, мыслить системно, осваивать параллельные функции
                    и
                    многое другое. Грамотно подобранный персональный ассистент – это не только правая рука руководителя,
                    это, можно сказать, второй человек в компании.</p>
            </div>
            <div class="paper-text text-style-paper d-flex flex-column align-items-end">
                <p>О том, как стать высокопрофессиональным персональным ассистентом, читайте в электронном журнале
                    «Делопроизводство в Казахстане».</p>
            </div> -->
        </div>
        <div class="paper-article-name-title">Похожие статьи</div>
        <div class="paper-card">
            <div class="entry-row">
            @foreach($posts as $post)
            <div class="entry-column">
                <a href="/post/{{$post->id}}">
                    <div class="item-entry-column">
                        <div class="entry-images">
                            <img src="/img/pictures/entry-img.png" alt="">
                        </div>
                        <div class="name-entry">{{$post->name}}</div>
                        <div class="text-entry">{{$post->bigname}}</div>
                        <div class="statistics-entry">
                            <div class="views-entry">
                                <img src="/img/icon/views-eyes.svg" alt="">
                                <span>55</span>
                            </div>
                            <div class="date-entry">
                                <span class="entry-mounth">Sep</span>
                                <span class="enrty-day">8</span>
                                <span class="entry-year">2020</span>
                            </div>
                        </div>
                    </div>
                </a>

            </div>
        @endforeach

        
            </div>
        </div>
    </div>
</div>

@endsection
