'use strict';

const {
    find, forInRight
} = require("lodash");

// Modal

const modalTrigger = document.querySelectorAll('[data-modal]'),
    modalSearch = document.querySelectorAll('[data-search]'),
    modal = document.querySelectorAll('.modal-overlay, .modal-login'),
    modalLinkReg = document.querySelectorAll('.modal-overlay, .modal-regist'),
	 modalLinkSearch = document.querySelectorAll('.modal-overlay, .modal-search'),
	 search = document.querySelector('.modal-search'),
	 modalReg = document.querySelector('[data-regist]'),
	 body = document.getElementsByClassName('body'),
	 searchItem = document.getElementsByClassName('search-item'),
	 accountItem = document.getElementsByClassName('account-item'),
	 modalCloseBtn = document.querySelectorAll('[data-close]'),
	 footerMenu = document.querySelector('.footer-menu');




modalTrigger.forEach(mdt => {
	mdt.addEventListener('click', function () {
		modal.forEach(open => {
			 open.classList.add('show');
			 open.classList.remove('hide');
			 document.body.style.overflow = 'hidden';
			 searchItem[0].style.zIndex = 1200;
		 accountItem[0].style.zIndex = 1210;
		 footerMenu.style.zIndex = 1199;
		});
		if (!search.classList.contains('hide')){
			search.classList.add('hide')
		}
  });
})


modalSearch.forEach(mds => {
	mds.addEventListener('click', function () {
		modalLinkSearch.forEach(find => {
			if(find.classList.contains('hide')){
				find.classList.remove('hide');
				footerMenu.style.zIndex = 1205;
			} else {
				find.classList.add('hide');
				footerMenu.style.zIndex = 1199;
			}
			searchItem[0].style.zIndex = 1210;
			accountItem[0].style.zIndex = 1200;

		});
		document.body.classList.toggle('bodyfixed');
	});
})


function closeRegist() {
    modalLinkReg.forEach(closemd => {
		closemd.classList.add('hide');
		closemd.classList.remove('show');
		  document.body.style.overflow = '';
		  modal.forEach(open => {
			open.classList.add('hide');
			open.classList.remove('show');
	  });
    });
}
function openRegist() {
    modal.forEach(open => {
        open.classList.add('hide');
        open.classList.remove('show');
        document.body.style.overflow = '';
        modalLinkReg.forEach(link => {
            link.classList.add('show');
            link.classList.remove('hide');
            document.body.style.overflow = 'hidden';
        });
    });
};



// function closeModal() {
// 	console.log('Error');
// 	modal.forEach(open => {
// 		 open.classList.add('hide');
// 		 open.classList.remove('show');
// 		 document.body.style.overflow = '';
// 	});
// }

modalReg.addEventListener('click', openRegist);
modalCloseBtn.forEach(closeModalWindow => {
    closeModalWindow.addEventListener('click', closeRegist);
});




//   if (find.contains(hide)) {
		// 	  console.log('err');
		// 	find.classList.toggle('hide');
		// 	} else {
				
		// 	}



//  Header ScrollTop



// let scrolltop = pageYOffset;
// const toggleClass = document.getElementById("navigation");

// window.addEventListener("scroll", function () {
//   if (pageYOffset > scrolltop) {
//     toggleClass.classList.add('navigation-color');
//   } else {
//     toggleClass.classList.remove('navigation-color');
//   }
//   scrolltop = pageYOffset;
// });


document.addEventListener("scroll", () => {
	let scrollTop = window.scrollY;
	const headerWrapper = document.querySelector(".navigation");
 
	if (scrollTop > 10) {
	  headerWrapper.classList.add("navigation-color");
	} else {
	  headerWrapper.classList.remove("navigation-color");
	}
 });