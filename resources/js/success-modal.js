'use strict'

const { BCardBody } = require("bootstrap-vue");

const successCheck = document.querySelectorAll('[data-check]'),
		checkModal = document.querySelectorAll('.password-update-overlay'),
		body = document.querySelector('body');


		successCheck.forEach( item => {
			item.addEventListener('click', function () {
				checkModal.forEach(event => {
					event.classList.toggle('hide');
			});
	});
});


const openBurger = document.querySelector('.burger-menu'),
		burger = document.querySelector('.burger-list');

openBurger.addEventListener('click', function () {
	openBurger.classList.toggle('active');
	burger.classList.toggle('active');
	document.body.classList.toggle('bodyfixed');
});