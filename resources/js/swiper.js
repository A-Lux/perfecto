import Swiper from 'swiper/bundle';

const sliderSwiper = document.querySelector('.container-slider-main-page');
let myySwiper = new Swiper(sliderSwiper, {
	 sliderPerView: 1,
	 loop: true,
	 effect: 'coverflow',
	 coverflowEffect: {
		rotate: 30,
		slideShadows: false,
	 },
    pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
	 },
	 autoplay: {
		delay: 5000,
		disableOnInteraction: false,
	 },
    //  navigation: {
    //      nextEl: '.swiper-button-next',
    //      prevEl: '.swiper-button-prev',
    //  },
});




const actionSwiper = document.querySelector('.container-slider-action');
let mySwiper = new Swiper(actionSwiper, {
	slidesPerView: 3,
	spaceBetween: 10,
	loop: true,
     navigation: {
         nextEl: '.swiper-button-next',
         prevEl: '.swiper-button-prev',
	  },
	  breakpoints: {
		// when window width is >= 360px
		320: {
			slidesPerView: 1,
			spaceBetween: 10
		 },
		 375: {
			slidesPerView: 1,
			spaceBetween: 10
		 },
		 768: {
			slidesPerView: 3,
			spaceBetween: 10
		 },
	 }
});


// var galleryTop = new Swiper('.gallery-top', {
// 	spaceBetween: 10,
// 	navigation: {
// 	  nextEl: '.swiper-button-next',
// 	  prevEl: '.swiper-button-prev',
// 	},
// 	// thumbs: {
// 	//   swiper: galleryThumbs
// 	// }	
//  });


var galleryThumbs = new Swiper('.gallery-thumbs', {
	direction: 'vertical',
	spaceBetween: 20,
	slidesPerView: 4,
	// freeMode: true,
	watchSlidesVisibility: true,
	// watchSlidesProgress: true,
	breakpoints: {
		// when window width is >= 360px
		320: {
			// slidesPerView: 1,
			// spaceBetween: 10
			direction: 'horizontal',
			spaceBetween: 10,
			slidesPerView: 4,
			freeMode: true,
		 },
		 375: {
			// slidesPerView: 1,
			// spaceBetween: 10
		 },
		 414: {
			// slidesPerView: 3,
			// spaceBetween: 10
			direction: 'horizontal',
			spaceBetween: 10,
			slidesPerView: 4,
			freeMode: true,
		 },
		 768: {
			// slidesPerView: 3,
			// spaceBetween: 10
			direction: 'vertical',
			spaceBetween: 20,
			slidesPerView: 4,
		 },
	 }
 });
 var galleryTop = new Swiper('.gallery-top', {
	thumbs: {
	  swiper: galleryThumbs
	}
 });