const overlay = document.querySelector('.modal-overlay-card'),
		closeModal = document.querySelectorAll('[data-exit]'),
		openModal = document.querySelector('[data-open]');
		if(openModal) {
			openModal.addEventListener('click', function () {
				overlay.classList.toggle('hide');
				document.body.style.overflow = 'hidden';
			});
		}

		closeModal.forEach(item => {
			item.addEventListener('click', function () {
				overlay.classList.toggle('hide');
				document.body.style.overflow = '';
		})
	});