const showCatalog = document.querySelector('[data-viewer]'),
		catalog = document.querySelector('[data-catalog]'),
		body = document.querySelector('body'),
		hideCatalog = document.querySelectorAll('[data-hide]');

		showCatalog.addEventListener('click', function() {
			catalog.classList.toggle('hide');
			body.classList.toggle('hidden');
		})


		hideCatalog.forEach(dif => {
			dif.addEventListener('click', function() {
				catalog.classList.toggle('hide');
				body.classList.toggle('hidden');
			})
		})
		