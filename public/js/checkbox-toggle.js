/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/checkbox-toggle.js":
/*!*****************************************!*\
  !*** ./resources/js/checkbox-toggle.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var onClick = document.querySelectorAll('.clickcheck');
var paystatus = document.querySelectorAll('.paystatus');
var last = 0;

var _loop = function _loop(x) {
  onClick[x].onclick = function () {
    if (last == x) {
      onClick[last].checked = true;
    } else {
      onClick[last].checked = false;
      paystatus[last].classList.add('checkedstatus');
      paystatus[x].classList.remove('checkedstatus');
    }

    last = x;
  };
};

for (var x = 0; x < onClick.length; x++) {
  _loop(x);
}

var itemIcon = document.querySelectorAll('.active-click-class');
itemIcon.forEach(function (item) {
  item.addEventListener('click', function () {
    itemIcon.forEach(function (item) {
      item.classList.remove('footer-active');
    });
    this.classList.add('footer-active');
  });
});
var checkShop = document.querySelectorAll('.checkshop'),
    toggleItemShop = document.querySelector('.toggle-item-shop');
toggleItemShop.addEventListener('click', function () {
  toggleItemShop.classList.toggle('item-close-shop');
  checkShop.forEach(function (check) {
    check.classList.toggle('invisiblewin');
  });
});
var onlinePay = document.querySelector('.payonline'),
    cashPay = document.querySelector('.paycash'),
    valueElement = document.querySelector('.basket-online');
onlinePay.addEventListener('click', function () {
  valueElement.value = onlinePay.value; // console.log(valueElement.value);
});
cashPay.addEventListener('click', function () {
  valueElement.value = cashPay.value; // console.log(valueElement.value);
});
var checkbox = document.querySelector('#toggle-switch'),
    toggleValue = document.querySelector('.toggle-value');
checkbox.addEventListener('change', function () {
  if (checkbox.checked) {
    //  console.log('Checked');
    checkbox.value = 1;
    toggleValue.value = checkbox.value; //  console.log(toggleValue.value);
  } else {
    // console.log('Not checked');
    checkbox.value = 0;
    toggleValue.value = checkbox.value; // console.log(toggleValue.value);
  }
});

/***/ }),

/***/ 4:
/*!***********************************************!*\
  !*** multi ./resources/js/checkbox-toggle.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\OSer\OpenServer\domains\Perfecto\resources\js\checkbox-toggle.js */"./resources/js/checkbox-toggle.js");


/***/ })

/******/ });