<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Subcategory;
class Category extends Model
{
    public $timestamps = false;

    public function getSubcategories() {
        // Subcategory
        return Category::where('parent_id', $this->id)->get();
    }

    public function getSubcategoriesIds() {
        // Subcategory
        return Subcategory::where('category_id', $this->id)->get('id')->toArray();
    }

    static function getCategories() {
        return Category::where('parent_id', null)->get();
    }

    static function getManCategories() {
        return Category::where('isMan', true)->where('parent_id',null)->get();
    }

    static function getWomanCategories() {
        return Category::where('isMan', false)->where('parent_id',null)->get();
    }

    public function subcategories()
    {
        return $this->hasMany('App\Subcategory');
    }
}