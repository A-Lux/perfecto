<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Attribute;

class Filter extends Model
{
    public function attributes() {

        return Attribute::where('filter_id', $this->id)->select('filter_id','value')->distinct('value')->get();
    }
}