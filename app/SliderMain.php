<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SliderMain extends Model
{
    protected $fillable = ['link', 'image'];
}
