<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Item;

class Search extends Component
{
    public $search = '';

    public function render()
    {
        return view('livewire.search', [
            'items' => Item::where('name', 'LIKE', '%'.$this->search.'%')->limit(7)->get(),
        ]);
    }
}
