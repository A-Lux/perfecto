<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Filter;
use App\Item;
use App\Attribute;
class ItemSort extends Component
{
    public $currentCategory;
    public $attributes = [];
    public $popular;
    public $sales;

    public function render()
    {
        return view('livewire.item-sort', [
            'filters' => Filter::where('category_id', $this->currentCategory->category_id)->get(),
            'items' => $this->sortItems(),
            'currentCategory' => $this->currentCategory
        ]);
    }

    public function sortItems() {
        if(count(array_filter($this->attributes)) == 0) {
            return Item::where('category_id', $this->currentCategory->id)->get();
        }
        $ids = array();
        foreach ($this->attributes as $key => $value) {
            $atr = Attribute::where('value', $key)->select('item_id')->get()->toArray();
            foreach ($atr as $id) {
                $ids[] = $id;
            }
        }
        return Item::whereIn('id', $ids)->where('category_id', $this->currentCategory->id)->get();
    }
}
