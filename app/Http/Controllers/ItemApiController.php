<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ItemCollection;
use App\Http\Resources\Item as ItemResource;
use App\Category;
use App\Item;
use App\Sale;

class ItemApiController extends Controller
{
    public function index() {
        return new ItemCollection(Item::all());
    }

    public function man() {
        $categories = Category::find(2)->getSubcategoriesIds();
        return new ItemCollection(Item::whereIn('category_id', $categories)->get());
    }

    public function woman() {
        $categories = Category::find(1)->getSubcategoriesIds();
        return new ItemCollection(Item::whereIn('category_id', $categories)->get());
    }

    public function show($id) {
        return new ItemResource(Item::find($id));
    }

    public function popular() {
        return new ItemCollection(Item::orderBy('created_at', 'desc')->limit(8)->get());
    }

    public function new() {
        return new ItemCollection(Item::orderBy('created_at', 'desc')->limit(8)->get());
    }

    public function isSale($id) {
        $check = Sale::where('item_id',$id)->first();
        if($check){
            return response()->json(['percents'=> $check->percents]);
        }
        else{
            return response()->json(['sale'=>'false']);
        }
    }
}
