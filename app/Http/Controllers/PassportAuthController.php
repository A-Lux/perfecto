<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class PassportAuthController extends Controller
{
    public function user_register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:4',
            'email' => 'required|email',
            'password' => 'required|min:8',
            'phone' => 'required',
            'birthdate' => 'required'
        ]);
 
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'birthdate' => $request->birthdate,
            'password' => bcrypt($request->password)
        ]);

        $token = $user->createToken('LaravelAuthApp')->accessToken;
 
        return response()->json(['user' => $user ,'token' => $token], 200);
    }
 
    /**
     * Login
     */
    public function login(Request $request)
    {
        $data = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (auth()->attempt($data)) {
            $token = auth()->user()->createToken('LaravelAuthApp')->accessToken;
            $user = User::where('id', auth()->user()->id)->select('id','name', 'email', 'phone', 'birthdate')->first();
            $wishlist = $user->wishlist()->select('item_id')->get();

            return response()->json(['user' => $user,'token' => $token, 'user_wishlist' => $wishlist], 200);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }   
}
