<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Category;
use App\Subcategory;
use App\Item;
use App\Article;
use App\SliderMain;

class MainController extends Controller
{
    public function index() {
        $categories = Category::all();
        $new = Item::inRandomOrder()->limit(5)->get();
        $popular = Item::inRandomOrder()->limit(5)->get();
        $onSale = Item::inRandomOrder()->limit(5)->get();
        $posts = Article::orderBy('views','desc')->limit(5)->get();
        $slider = SliderMain::all();
        return view('home', compact('categories', 'new', 'popular','onSale','posts','slider'));
    }

    public function catalog_all() {
        $categories = Category::getCategories();
        return view('categories.index', compact('categories'));
    }
    
    public function basket() {
        return view('items.basket');
    }

    public function catalog($id) {
        $categories = Category::getWomanCategories();
        $currentCategory = Subcategory::find($id);
        $items = Item::where('category_id', $id)->paginate(12);
        return view('categories.show', compact('categories', 'currentCategory', 'items'));
    }
}