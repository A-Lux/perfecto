<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;

class ArticleController extends Controller
{
    public function index(){
        $posts = Article::all();
        return view('articles.index', compact('posts'));
    }

    public function show($id){
        $post = Article::find($id);
        $post->update(['views' => $post->views + 1]);
        $posts = Article::all();
        return view('articles.show', compact('post','posts'));
    }

}
