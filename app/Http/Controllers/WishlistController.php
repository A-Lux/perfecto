<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Wishlist;
use App\Http\Resources\ItemCollection;
use App\Item;

class WishlistController extends Controller
{
    public function index() {
        $wishlist = auth()->user()->wishlist()->select('item_id')->get()->toArray();

        return new ItemCollection(Item::whereIn('id', $wishlist)->get());
    }

    public function store(Request $request) {
        Wishlist::create([
            'user_id' => auth()->user()->id,
            'item_id' => $request->item_id
        ]);

        $wishlist = auth()->user()->wishlist()->select('item_id')->get();
        
        return response()->json(['user_wishlist' => $wishlist]);
    }

    public function destroy($id) {
        Wishlist::where('item_id', $id)->first()->delete();
        $wishlist = auth()->user()->wishlist()->select('item_id')->get();
        return response()->json(['user_wishlist' => $wishlist]);
    }
}
