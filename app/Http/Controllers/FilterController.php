<?php

namespace App\Http\Controllers;

use App\Filter;
use App\Attribute;
use App\Item;
use App\Http\Resources\ItemCollection;

use Illuminate\Http\Request;

class FilterController extends Controller
{

    public function index($id)
    {
        $filters = Filter::where('category_id', $id)->get();
        return response()->json(['data' => $filters]);
    }

    public function show($id)
    {
        $filters = Attribute::where('filter_id', $id)->select('filter_id','value')->distinct('value')->get();
        return response()->json(['data' => $filters]);
    }

    public function sort(Request $request)
    {
        $ids = array();
        foreach ($request->all() as $key => $value) {
            $atr = Attribute::where('value', $value)->select('item_id')->get()->toArray();
            foreach ($atr as $id) {
                $ids[] = $id;
            }
        }
        if($request->from) {
            return new ItemCollection(Item::whereIn('id', $ids)->whereBetween('price', [$request->from, $request->to])->get());
        }
        return new ItemCollection(Item::whereIn('id', $ids)->where('category_id', $request->category_id)->get());
    }

    public function sort_price(Request $request)
    {
        return new ItemCollection(Item::whereBetween('price', [$request->from, $request->to])->where('category_id', $request->category_id)->get());
    }
}
