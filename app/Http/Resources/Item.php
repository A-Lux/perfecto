<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Item extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'image' => '/storage/' . str_replace("\\", "/", $this->image),
            'slider_images' => json_decode($this->images),
            'price' => $this->price,
            'sizes' => $this->sizes,
            'isAvailable' => $this->isAvailable,
            'category_id' => $this->category_id,
            'created_at' => $this->created_at,
        ];
    }
}
