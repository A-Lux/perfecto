<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Post extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'cover_image' => '/storage/' . str_replace("\\", "/", $this->cover_image),
            'text' => $this->text,
            'views' => $this->views,
            'created_at' => $this->created_at,
        ];
    }
}
