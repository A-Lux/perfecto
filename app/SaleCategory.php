<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Sale;
class SaleCategory extends Model
{
    

    public function sales()
    {
        return $this->hasMany('App\Sale');
    }

    
    public function getSalesItemsIds() 
    {
        return Sale::where('sale_category_id', $this->id)->get('item_id')->toArray();
    }
}
