<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    public function items()
    {
        return $this->hasMany('App\Item', 'category_id');
    }
}
