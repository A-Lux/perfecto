const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.js('resources/js/jquery-3.5.1.min.js', 'public/js')
.js('resources/js/app.js', 'public/js')
.js('resources/js/modal.js', 'public/js')
.js('resources/js/swiper.js', 'public/js')
.js('resources/js/checkbox-toggle.js', 'public/js')
.js('resources/js/add-border.js', 'public/js')
.js('resources/js/showPassword.js', 'public/js')
.js('resources/js/tableModal.js', 'public/js')
.js('resources/js/success-modal.js', 'public/js')
.js('resources/js/type-thing.js', 'public/js')
.js('resources/js/catalog-viewer.js', 'public/js')
.js('resources/js/collapse.js', 'public/js')
.sass('resources/sass/app.scss', 'public/css')
.sass('resources/sass/pages/delivery.scss', 'public/css')
.sass('resources/sass/pages/exchange.scss', 'public/css')
.sass('resources/sass/pages/categories.scss', 'public/css')
.sass('resources/sass/pages/about.scss', 'public/css')
.sass('resources/sass/pages/home.scss', 'public/css')
.sass('resources/sass/pages/entry.scss', 'public/css')
.sass('resources/sass/pages/paper.scss', 'public/css')
.sass('resources/sass/pages/action.scss', 'public/css')
.sass('resources/sass/pages/card-things.scss', 'public/css')
.sass('resources/sass/pages/cabinet.scss', 'public/css')
.sass('resources/sass/pages/cabinet-date.scss', 'public/css')
.sass('resources/sass/pages/cabinet-pass.scss', 'public/css')
.sass('resources/sass/pages/cabinet-mail.scss', 'public/css')
.sass('resources/sass/pages/cabinet-mail-rep.scss', 'public/css')
.sass('resources/sass/pages/basket.scss', 'public/css')
.sass('resources/sass/pages/catalog.scss', 'public/css');
